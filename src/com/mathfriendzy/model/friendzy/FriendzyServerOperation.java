package com.mathfriendzy.model.friendzy;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import com.mathfriendzy.utils.CommonUtils;
import com.mathfriendzy.utils.ICommonUtils;


public class FriendzyServerOperation 
{	
	private ArrayList<FriendzyDTO> frndzyList;
	private ArrayList<ChallengerDTO> challengerList;

	public FriendzyServerOperation()
	{
		frndzyList = new ArrayList<FriendzyDTO>();
		challengerList = new ArrayList<ChallengerDTO>();
	}

	/**
	 * use to get challenges from teacher and student
	 * @param userId
	 * @param playerId
	 * @param action
	 * @return list of FriendzyChallenge with userid or playerId
	 */
	public ArrayList<FriendzyDTO> getFriendzyForPlayer(String userId, String playerId, Boolean isTeacher)
	{
		String strUrl;
		String action;
		if(!isTeacher)
		{
			action = "getFriendzyChallengesForPlayer";
			strUrl = ICommonUtils.COMPLETE_URL +"action="+ action
					+"&userId="+userId+"&playerId="+playerId+
					"&appId="+CommonUtils.APP_ID;			
		}
		else
		{
			action = "getFriendzyChallenges";
			strUrl = ICommonUtils.COMPLETE_URL +"action="+ action
					+"&teacherId="+userId+
					"&appId="+CommonUtils.APP_ID;
		}
		//Log.e("getFriendzyChallenges", ""+strUrl);
		parseJsonForPlayerData(CommonUtils.readFromURL(strUrl));

		return frndzyList;
	}


	/**
	 * This method parse json String which contain player detail
	 * @param jsonString
	 */
	private void parseJsonForPlayerData(String jsonString)
	{		
		//Log.e("FriendzyServerOperation", ""+jsonString);

		try 
		{
			JSONObject jsonObj = new JSONObject(jsonString);		
			JSONArray jsonArray	= jsonObj.getJSONArray("data");

			for(int i =0; i < jsonArray.length(); i++)
			{
				JSONObject obj = jsonArray.getJSONObject(i);

				FriendzyDTO dto = new FriendzyDTO();
				dto.setChallengerId(obj.getString("challengeId"));
				dto.setClassName(obj.getString("className"));
				dto.setEndDate(obj.getString("endDate"));
				dto.setEquations(obj.getString("equations"));
				dto.setGrade(obj.getString("grade"));
				dto.setRewards(obj.getString("rewards"));
				dto.setSchoolId(obj.getString("schoolId"));
				dto.setSchoolName(obj.getString("schoolName"));
				dto.setStartDate(obj.getString("startDate"));
				dto.setStatus(obj.getString("status"));
				dto.setTeacherId(obj.getString("teacherId"));
				dto.setTeacherName(obj.getString("teacherName"));
				dto.setType(obj.getString("type"));

				frndzyList.add(dto);

			}

		} 
		catch (JSONException e) 
		{			
			Log.e("No Friendzy Challenge: ", ""+e.toString());
		}
	}



	public ArrayList<ChallengerDTO> getFriendzyForUser(String challengerId)
	{
		String action = "getFriendzyChallengePlayers";

		String strUrl = ICommonUtils.COMPLETE_URL +"action="+ action
				+"&challengeId="+challengerId+
				"&appId="+CommonUtils.APP_ID;
		//Log.e("getFriendzyForPlayer", ""+strUrl);

		parseJsonForChallenger(CommonUtils.readFromURL(strUrl));

		return challengerList;
	}

	private void parseJsonForChallenger(String jsonString)
	{
		try 
		{
			JSONObject jsonObj = new JSONObject(jsonString);		
			JSONArray jsonArray	= jsonObj.getJSONArray("data");

			for(int i =0; i < jsonArray.length(); i++)
			{
				JSONObject obj = jsonArray.getJSONObject(i);
				ChallengerDTO dto = new ChallengerDTO();

				dto.setImageId(obj.getString("imageId"));
				dto.setName(obj.getString("name"));
				dto.setPlayerId(obj.getString("playerId"));
				dto.setPoints(obj.getString("points"));
				dto.setTeacherId(obj.getString("teacherId"));
				dto.setTime(obj.getString("time"));
				dto.setUserId(obj.getString("userId"));

				challengerList.add(dto);
			}
		}
		catch (JSONException e) 
		{			
			Log.e("No Friendzy Challenge: ", ""+e.toString());
		}

	}


	public String createChallenge(FriendzyDTO dto)
	{
		String challengeId = null;
		String action 		= "createFriendzyChallenge";
		ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();   	    
		try 
		{
			nameValuePairs.add(new BasicNameValuePair("action", action));
			nameValuePairs.add(new BasicNameValuePair("appId", CommonUtils.APP_ID));
			nameValuePairs.add(new BasicNameValuePair("challengeId", dto.getChallengerId()));
			nameValuePairs.add(new BasicNameValuePair("teacherId", dto.getTeacherId()));
			nameValuePairs.add(new BasicNameValuePair("teacherName",dto.getTeacherName()));
			nameValuePairs.add(new BasicNameValuePair("schoolId", dto.getSchoolId()));
			nameValuePairs.add(new BasicNameValuePair("schoolName", dto.getSchoolName()));
			nameValuePairs.add(new BasicNameValuePair("startDate", dto.getStartDate()));
			nameValuePairs.add(new BasicNameValuePair("endDate", dto.getEndDate()));
			nameValuePairs.add(new BasicNameValuePair("status", dto.getStatus()));
			nameValuePairs.add(new BasicNameValuePair("className", dto.getClassName()));
			nameValuePairs.add(new BasicNameValuePair("type", dto.getType()));
			nameValuePairs.add(new BasicNameValuePair("grade", dto.getGrade()));
			nameValuePairs.add(new BasicNameValuePair("equations", dto.getEquations()));
			nameValuePairs.add(new BasicNameValuePair("rewards", dto.getRewards()));
		}
		catch (Exception e1) 
		{			
			Log.e("Register", "Error in addMathPlayLevelScore While parsing" + e1);
		}

		//Log.e("Register inseide update user on server", nameValuePairs.toString());

		//resultValue = this.parseJson(CommonUtils.readFromURL(strURL));
		challengeId = this.parseJson(CommonUtils.readFromURL(nameValuePairs));
		return challengeId;
	}

	private String parseJson(String readFromURL)
	{	
		//Log.e("readFromURL", ""+readFromURL);		
		try 
		{
			JSONObject jsonObj = new JSONObject(readFromURL);
			return jsonObj.getString("data");
		} catch (JSONException e) {
			return null;
		}

	}

	/**
	 * use to delete challenge by Teacher
	 * @param challengeId
	 */
	public void deleteChallenge(String challengeId)
	{
		String action = "deleteFriendzyChallenge";
		String strUrl = ICommonUtils.COMPLETE_URL+"action="+action
				+"&challengeId="+challengeId+"&appId="+CommonUtils.APP_ID;

		this.parseJson(CommonUtils.readFromURL(strUrl));

	}

	/**
	 * use to delete player challenge
	 * @param userId
	 * @param playerId
	 * @param challengeId
	 */
	public String deletePlayerChallenge(String userId, String playerId,String challengeId)
	{
		//String action = "deletePlayerFromFriendzyChallenge";
		String action = "deletePlayerFromFriendzyChallengeAllDevices";
		String strUrl = ICommonUtils.COMPLETE_URL+"action="+action
				+"&challengeId="+challengeId
				+"&userId="+userId+"&playerId="+playerId
				+"&appId="+CommonUtils.APP_ID;

		return CommonUtils.readFromURL(strUrl);

	}


	/**
	 * use to save record for Friendzy Challenge
	 * @param userId
	 * @param playerId
	 * @param challengeId
	 * @param time
	 * @param points
	 */
	public void saveTimeForFriendzy(String userId, String playerId, String challengeId, int time, int points)
	{
		String action = "saveTimePointsForFriendzyChallenge";
		String strUrl =  ICommonUtils.COMPLETE_URL+"action="+action
				+"&challengeId="+challengeId
				+"&userId="+userId+"&playerId="+playerId
				+"&time="+time+"&points="+points
				+"&appId="+CommonUtils.APP_ID;

		this.parseJson(CommonUtils.readFromURL(strUrl));		
	}

	//http://192.168.1.25/~deepak/LeapAhead/Trivia/index.php?action=getAllDevicePidsForChallenge&challengeId=16&appId=7

	public String[] getDevicePid(String challengeId)
	{
		String[] strPid;

		String strUrl =  ICommonUtils.COMPLETE_URL+"action=getAllDevicePidsForChallenge" +
				"&challengeId="+challengeId+"&appId="+CommonUtils.APP_ID;

		//Log.e("", ""+strUrl);
		strPid = this.parseJsonForPid(CommonUtils.readFromURL(strUrl));
		return strPid;
	}


	private String[] parseJsonForPid(String readFromURL)
	{		
		//Log.e("", ""+readFromURL);
		String[] strPid = new String[2];
		try 
		{
			JSONObject jsonObj = new JSONObject(readFromURL);
			strPid[0] = jsonObj.getString("data");
			strPid[1] = jsonObj.getString("androidPids");
			//Log.e("", "strPid[1] "+strPid[1]);
		} 
		catch (JSONException e){

		}

		return strPid;

	}


	public void createNotificationForChallenge(FriendzyDTO dto,String devicePid)
	{	
		dto.getClassName().replace("~", "");	
		dto.getTeacherName().replace("~", "");
		dto.getSchoolName().replace("~", "");

		String teahcer = "";
		String className = "";
		String School	= "";

		try 
		{
			teahcer   = URLEncoder.encode(dto.getTeacherName(), "UTF-8");
			className   = URLEncoder.encode(dto.getClassName(), "UTF-8");
			School   = URLEncoder.encode(dto.getSchoolName(), "UTF-8");			

		} 
		catch (UnsupportedEncodingException e) 
		{
			Log.e("Register", "Error:Unicode String Exception");
		}
		String friendzy = dto.getChallengerId()+"~"+dto.getTeacherId()+"~"+
				teahcer+"~"+dto.getSchoolId()+"~"+
				School+"~"+dto.getStartDate()+"~"+
				dto.getEndDate()+"~"+dto.getStatus()+"~"+
				className+"~"+dto.getType()+"~"+dto.getGrade();

		String strUrl = ICommonUtils.COMPLETE_URL_FOR_REG_DEVICE_NITFICATION+
				"&action=createChallenge&devicePids="+devicePid
				+"&teacher="+teahcer+"&friendzyChallenge="+friendzy;

		//Log.e("CreATE Challenge", ""+strUrl);
		this.parseJson(CommonUtils.readFromURL(strUrl));	
	}


	public void createNotificationForDeleteChallenge(FriendzyDTO dto,String devicePid, String pName, String uid, String pid)
	{
		dto.getClassName().replace("~", "");	
		dto.getTeacherName().replace("~", "");

		String teahcer = "";
		String className = "";
		try 
		{
			teahcer   	= URLEncoder.encode(dto.getTeacherName(), "UTF-8");
			className   = URLEncoder.encode(dto.getClassName(), "UTF-8");
			pName		= URLEncoder.encode(pName, "UTF-8");
		} 
		catch (UnsupportedEncodingException e) 
		{
			Log.e("Register", "Error:Unicode String Exception");
		}
		String friendzy = dto.getChallengerId()+"~"+teahcer+"~"+dto.getStartDate()+"~"+dto.getEndDate()+"~"+className;

		String strUrl = ICommonUtils.COMPLETE_URL_FOR_REG_DEVICE_NITFICATION+
				"&action=deletePlayer" +
				"&devicePids="+devicePid+
				"&playerName="+pName+
				"&userId="+uid+
				"&playerId="+pid+
				"&friendzyChallenge="+friendzy;

		//Log.e("Delete Challenge", ""+strUrl);
		this.parseJson(CommonUtils.readFromURL(strUrl));
	}

}
