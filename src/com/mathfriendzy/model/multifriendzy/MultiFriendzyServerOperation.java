package com.mathfriendzy.model.multifriendzy;

import static com.mathfriendzy.utils.ICommonUtils.COMPLETE_URL;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.model.singleFriendzy.EquationFromServerTransferObj;
import com.mathfriendzy.utils.CommonUtils;
import com.mathfriendzy.utils.ICommonUtils;


/**
 * This class do the server oparation for the multifrindzy
 * @author Yashwant Singh
 *
 */
public class MultiFriendzyServerOperation 
{
	/**
	 * This method get the friendzys for the player
	 * @param userId
	 * @param playerId
	 * @param offset
	 */
	public ArrayList<MultiFriendzysFromServerDTO> getFriendzysForPlayer(String userId , String playerId , int offset)
	{
		//String action = "getFriendzysForPlayer";
		String action = "getFriendzysForPlayerAndroid";
		String strUrl = null;

		/*if(offset == 0)
		{
			strUrl = COMPLETE_URL  + "action=" + action + "&"
					+ "userId="   + userId  + "&"
					+ "playerId=" + playerId;
		}
		else
		{
			strUrl = COMPLETE_URL  + "action=" + action + "&"
					+ "userId="   + userId  + "&"
					+ "playerId=" + playerId + "&"
					+ "offset=" + offset;
		}*/

		if(offset == 0)
		{
			strUrl = ICommonUtils.CONPLETE_URL_FOR_NOTIFICATION  + "action=" + action + "&"
					+ "userId="   + userId  + "&"
					+ "playerId=" + playerId + "&"
					+ "appId=" + CommonUtils.APP_ID;
		}
		else
		{
			strUrl = ICommonUtils.CONPLETE_URL_FOR_NOTIFICATION  + "action=" + action + "&"
					+ "userId="   + userId  + "&"
					+ "playerId=" + playerId + "&"
					+ "offset=" + offset + "&"
					+ "appId=" + CommonUtils.APP_ID;
		}

		//Log.e("MultiFriendzyServerOperation", "inside getFriendzysForPlayer url : " +  strUrl);

		return this.parseFriendzysplayerJson(CommonUtils.readFromURL(strUrl));
	}

	private ArrayList<MultiFriendzysFromServerDTO> parseFriendzysplayerJson(String jsonString)
	{	
		//Log.e("MultiFriendzyServerOperation", "inside parseFriendzysplayerJson " + jsonString);

		ArrayList<MultiFriendzysFromServerDTO> mathFriedzysList = new ArrayList<MultiFriendzysFromServerDTO>();

		try 
		{
			JSONObject jObject = new JSONObject(jsonString);

			JSONArray jsonArrayObj = jObject.getJSONArray("data");

			for( int i = 0 ; i < jsonArrayObj.length() ; i ++ )
			{
				JSONObject jsonDataObj = jsonArrayObj.getJSONObject(i);

				MultiFriendzysFromServerDTO multFriendzysDataObj = new MultiFriendzysFromServerDTO();
				multFriendzysDataObj.setCountryCode(jsonDataObj.getString("country"));
				multFriendzysDataObj.setNotificationDevices(jsonDataObj.getString("notificationDevices"));
				//multFriendzysDataObj.setAndroidPids(jsonDataObj.getString("androidPids"));
				multFriendzysDataObj.setFriendzysId(jsonDataObj.getString("friendzyId"));
				multFriendzysDataObj.setType(jsonDataObj.getString("type"));
				multFriendzysDataObj.setInitiatorId(jsonDataObj.getString("initiator"));
				multFriendzysDataObj.setStartingFriendzyDate(jsonDataObj.getString("date"));
				multFriendzysDataObj.setIsCompleted(jsonDataObj.getInt("isCompleted"));
				multFriendzysDataObj.setWinner(jsonDataObj.getInt("winner"));

				//get oppenent data
				JSONObject jsonDataOpponentDataObj = jsonDataObj.getJSONObject("opponent");

				OppnentDataDTO opponentData = new OppnentDataDTO();

				opponentData.setfName(jsonDataOpponentDataObj.getString("fName"));
				opponentData.setlName(jsonDataOpponentDataObj.getString("lName"));
				opponentData.setSchoolId(jsonDataOpponentDataObj.getString("schoolId"));
				opponentData.setSchoolName(jsonDataOpponentDataObj.getString("schoolName"));
				opponentData.setGrade(jsonDataOpponentDataObj.getString("grade"));
				opponentData.setTeacherUserid(jsonDataOpponentDataObj.getString("teacherUserId"));
				opponentData.setTeacherFirstName(jsonDataOpponentDataObj.getString("teacherFirstName"));
				opponentData.setTeacherLastName(jsonDataOpponentDataObj.getString("teacherLastName"));
				opponentData.setIndexOfAppearance(jsonDataOpponentDataObj.getString("indexOfAppearance"));
				opponentData.setParentUserId(jsonDataOpponentDataObj.getString("parentUserId"));
				opponentData.setPlayerId(jsonDataOpponentDataObj.getString("playerId"));
				opponentData.setCompleteLevel(jsonDataOpponentDataObj.getString("competeLevel"));
				opponentData.setProfileImageNameId(jsonDataOpponentDataObj.getString("profileImageId"));
				opponentData.setCoins(jsonDataOpponentDataObj.getString("coins"));
				opponentData.setPoints(jsonDataOpponentDataObj.getString("points"));
				opponentData.setCity(jsonDataOpponentDataObj.getString("city"));
				opponentData.setState(jsonDataOpponentDataObj.getString("state"));
				opponentData.setUserName(jsonDataOpponentDataObj.getString("userName"));


				multFriendzysDataObj.setOpponentData(opponentData);

				//for round info
				ArrayList<MathFriendzysRoundDTO> roundList = new ArrayList<MathFriendzysRoundDTO>();

				JSONArray jsonRoundArrayObj = jsonDataObj.getJSONArray("rounds");

				for( int j = 0 ; j < jsonRoundArrayObj.length() ; j ++ )
				{
					JSONObject jsonRoundObj = jsonRoundArrayObj.getJSONObject(j);

					MathFriendzysRoundDTO roundData = new MathFriendzysRoundDTO();
					roundData.setRoundNumber(jsonRoundObj.getString("roundNum"));
					roundData.setPlayerScore(jsonRoundObj.getString("plrScore"));
					roundData.setOppScore(jsonRoundObj.getString("oppScore"));
					roundList.add(roundData);
				}

				multFriendzysDataObj.setRoundList(roundList);

				mathFriedzysList.add(multFriendzysDataObj);
			}
		}
		catch (JSONException e) 
		{			
			e.printStackTrace();
		}

		return mathFriedzysList;
	}

	/**
	 * This method get the equations from server
	 * @param userId
	 * @param playerId
	 * @param friendzyI
	 */
	public ArrayList<EquationFromServerTransferObj> findMultiFriendzyEquatiosForPlayer(String userId ,
			String playerId , String friendzyId){

		String action = "findMultiFriendzyEquatiosForPlayer";
		String strUrl = COMPLETE_URL  + "action=" + action + "&"
				+ "userId="   + userId  + "&"
				+ "playerId=" + playerId + "&"
				+ "friendzyId=" + friendzyId;

		//Log.e("MultiFriendzyServerOperation", "url : " + strUrl);

		return this.parseJsonFormultiFriendzyEquations(CommonUtils.readFromURL(strUrl));
	}

	/**
	 * This method parse the json of equations
	 * @param jsonString
	 */
	private ArrayList<EquationFromServerTransferObj> parseJsonFormultiFriendzyEquations(String jsonString)
	{
		//Log.e("MultiFriendzyServerOperation", "Json String  : " + jsonString);

		ArrayList<EquationFromServerTransferObj> equationList = new ArrayList<EquationFromServerTransferObj>();
		try 
		{
			JSONObject jObject = new JSONObject(jsonString);
			JSONArray jsonObject2 = jObject.getJSONArray("data");

			for(int i = 0 ; i < jsonObject2.length() ; i ++ )
			{
				JSONObject jsonObj = jsonObject2.getJSONObject(i);

				EquationFromServerTransferObj equationObj = new EquationFromServerTransferObj();
				equationObj.setUserId(jsonObj.getString("userId"));
				equationObj.setPlayerId(jsonObj.getString("playerId"));
				//equationObj.setSetIsFakePlayer((jsonObj.getString("isFakePlayer")));
				equationObj.setMathEquationId(jsonObj.getInt("questionId"));
				equationObj.setOperationId(jsonObj.getInt("categoryId"));
				//equationObj.setLap(jsonObj.getInt("lap"));
				equationObj.setPoints(jsonObj.getInt("points"));
				equationObj.setIsAnswerCorrect(jsonObj.getInt("isAnswerCorrect"));
				equationObj.setTimeTakenToAnswer(jsonObj.getDouble("timeTakenToAnswer"));

				equationList.add(equationObj);
			}

			return equationList;
		}
		catch (JSONException e) 
		{
			Log.e("MultiFriendzyServerOperation", "No Equation exist Score exist :" + e.toString());
			return null;
		}
	}


	/**
	 * This method create friendzy on server
	 */
	public String createMultiFriendzy(CreateMultiFriendzyDto createMultiFrinedzyDto, boolean isAddParameter) 
	{
		String action = "createMultiFriendzy";

		/*Log.e("MultiFriendzyServerOperation", "inside url info friendzyId" + createMultiFrinedzyDto.getFriendzyId() + "\n"
				+ "userId " + createMultiFrinedzyDto.getPlayerUserId() + "\n"
				+ " playerId " + createMultiFrinedzyDto.getPlayerId() + "\n"
				+ "useroppentnId " + createMultiFrinedzyDto.getOpponentPlayerId() + "\n"
				+ " opponent parent id " + createMultiFrinedzyDto.getOpponentUserId()
				+ " \n score " + createMultiFrinedzyDto.getRoundScore() + "\n" 
				+ " total points " + createMultiFrinedzyDto.getPoints()
				+ " date " + createMultiFrinedzyDto.getDate() + "\n" 
				+ " modifying " + 	createMultiFrinedzyDto.getModifying() + "\n"
				+ " isCompleted " + createMultiFrinedzyDto.getIsCompleted() + "\n"
				+ " winner " + createMultiFrinedzyDto.getWinner() + " \n" 
				+ " shouldSaveEquations = " + createMultiFrinedzyDto.getShouldSaveEquations());

		Log.e("MultiFriendzyServerOperation", " problems " + createMultiFrinedzyDto.getProblems());*/

		ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

		try 
		{
			nameValuePairs.add(new BasicNameValuePair("action", action));
			nameValuePairs.add(new BasicNameValuePair("problems", "<equations>" + createMultiFrinedzyDto.getProblems() + "</equations>"));
			nameValuePairs.add(new BasicNameValuePair("friendzyId", createMultiFrinedzyDto.getFriendzyId()));
			nameValuePairs.add(new BasicNameValuePair("userId", createMultiFrinedzyDto.getPlayerUserId()));
			nameValuePairs.add(new BasicNameValuePair("playerId", createMultiFrinedzyDto.getPlayerId()));
			nameValuePairs.add(new BasicNameValuePair("userIdOpponent", createMultiFrinedzyDto.getOpponentUserId()));
			nameValuePairs.add(new BasicNameValuePair("opponentId", createMultiFrinedzyDto.getOpponentPlayerId()));
			nameValuePairs.add(new BasicNameValuePair("score", createMultiFrinedzyDto.getRoundScore() + ""));
			nameValuePairs.add(new BasicNameValuePair("totalPoints", createMultiFrinedzyDto.getPoints() + ""));
			nameValuePairs.add(new BasicNameValuePair("coins", createMultiFrinedzyDto.getCoins() + ""));
			nameValuePairs.add(new BasicNameValuePair("date", createMultiFrinedzyDto.getDate()));
			nameValuePairs.add(new BasicNameValuePair("appId", CommonUtils.APP_ID));

			if(isAddParameter)
			{
				nameValuePairs.add(new BasicNameValuePair("modifying", createMultiFrinedzyDto.getModifying() + ""));
				nameValuePairs.add(new BasicNameValuePair("isCompleted", createMultiFrinedzyDto.getIsCompleted() + ""));
				nameValuePairs.add(new BasicNameValuePair("winner", String.valueOf(createMultiFrinedzyDto.getWinner())));
			}

			nameValuePairs.add(new BasicNameValuePair("shouldSaveEquations",
					createMultiFrinedzyDto.getShouldSaveEquations() + ""));

		}
		catch (Exception e1) 
		{			
			Log.e("Register", "Error in addMathPlayLevelScore While parsing" + e1);
		}
		//Log.e("Create MultiFriendzy", ""+nameValuePairs.toString());
		return this.parseCreateMultiFriendzyJson(CommonUtils.readFromURL(nameValuePairs));	
	}

	/**
	 * This method parse create multifriendzy json string
	 * @param jsonString
	 */
	private String parseCreateMultiFriendzyJson(String jsonString)
	{
		//Log.e("MultiFriendzyServerOperation", " inside parseCreateMultiFriendzyJson  json String : " + jsonString);

		try 
		{
			JSONObject jObject = new JSONObject(jsonString);
			return jObject.getString("friendzyId");
		}
		catch (Exception e) 
		{
			Log.e("MultiFriendzyServerOperation", "No FriendzyID exist exist :" + e.toString());
			return null;
		}
	}

	public void resignAndSaveFriendzy(String friendzyId , String winnerId)
	{
		String action = "resignAndSaveFriendzy";
		String strUrl = COMPLETE_URL  + "action=" + action + "&"
				+ "frId="   + friendzyId  + "&"
				+ "winnerId=" + winnerId+"&appId="+CommonUtils.APP_ID ;

		//Log.e("MultiFriendzyServerOperation", "inside resignAndSaveFriendzy url : " + strUrl);

		this.parseResignAndSaveFriendzy(CommonUtils.readFromURL(strUrl));
	}

	/**
	 * This  method parse the json String for resign and save friendzy
	 * @param jsonString
	 */
	private void parseResignAndSaveFriendzy(String jsonString)
	{
		//Log.e("MultiFriendzyServerOperation", " inside parseResignAndSaveFriendzy  json String : " + jsonString);
	}

	/**
	 * This method check the player want to play with already exist player
	 * @param userId
	 * @param playerId
	 * @param opponentUserId
	 * @param opponentPlayerId
	 */
	public int checkFriendzyExistsForPlayers(String userId , String playerId , String opponentUserId , String opponentPlayerId)
	{
		String action = "CheckFriendzyExistsForPlayers";
		String strUrl = COMPLETE_URL  + "action=" + action + "&"
				+ "userId1="   + userId  + "&"
				+ "playerId1=" + playerId     + "&"
				+ "userId2="   + opponentUserId + "&"
				+ "playerId2=" + opponentPlayerId+"&appId="+CommonUtils.APP_ID;

		//Log.e("MultiFriendzyServerOperation", "inside checkFriendzyExistsForPlayers url : " + strUrl);

		return this.parsecheckFriendzyExistsForPlayers(CommonUtils.readFromURL(strUrl));
	}

	/**
	 * This method parse json String for the existint player 
	 * @param jsonString
	 */
	private int parsecheckFriendzyExistsForPlayers(String jsonString)
	{
		//Log.e("MultiFriendzyServerOperation", " inside parsecheckFriendzyExistsForPlayers  json String : " + jsonString);

		try 
		{
			JSONObject jObject = new JSONObject(jsonString);
			return jObject.getInt("data");
		}
		catch (JSONException e) 
		{
			Log.e("MultiFriendzyServerOperation", "No FriendzyID exist exist :" + e.toString());
			return 1;
		}
	}

	/**
	 * This method get the round data from server when user fro notification
	 * @param userId
	 * @param playerId
	 * @param friendzyId
	 */
	public MultiFriendzysFromServerDTO getRoundList(String userId , String playerId ,String friendzyId)
	{
		String action = "findMultiFriendzyRounds";
		String strUrl = COMPLETE_URL  + "action=" + action + "&"
				+ "userId="   + userId  + "&"
				+ "playerId=" + playerId     + "&"
				+ "friendzyId="   + friendzyId+"&appId="+CommonUtils.APP_ID ;

		Log.e("SingleFriendzyServerOperation", "inside getRoundList url : " + strUrl);
		return this.parseJsonStringForRoundData(CommonUtils.readFromURL(strUrl));
	}

	/**
	 * This method parse round son string
	 * @param jsonString
	 */
	private MultiFriendzysFromServerDTO parseJsonStringForRoundData(String jsonString)
	{
		MultiFriendzysFromServerDTO serverDataObj = new MultiFriendzysFromServerDTO();

		//Log.e("SingleFriendzyServerOperation", " inside parseJsonStringForRoundData " + jsonString);

		try 
		{
			JSONObject jObject = new JSONObject(jsonString);
			JSONObject jsonObject2 = jObject.getJSONObject("data");

			serverDataObj.setFriendzysId(jsonObject2.getString("friendzyId"));
			serverDataObj.setType(jsonObject2.getString("type"));
			serverDataObj.setInitiatorId(jsonObject2.getString("initiator"));
			serverDataObj.setWinner(jsonObject2.getInt("winner"));

			JSONArray roundArray = jsonObject2.getJSONArray("rounds");

			ArrayList<MathFriendzysRoundDTO> roundList = new ArrayList<MathFriendzysRoundDTO>();

			for( int i = 0 ; i < roundArray.length() ; i ++ )
			{
				JSONObject jsonObj = roundArray.getJSONObject(i);
				MathFriendzysRoundDTO roundObj = new MathFriendzysRoundDTO();

				roundObj.setRoundNumber(jsonObj.getString("roundNum"));
				roundObj.setPlayerScore(jsonObj.getString("plrScore"));
				roundObj.setOppScore(jsonObj.getString("oppScore"));

				roundList.add(roundObj);
			}

			serverDataObj.setRoundList(roundList);
		}
		catch (JSONException e) 
		{
			Log.e("MultiFriendzyServerOperation", "No FriendzyID exist exist :" + e.toString());
		}

		return serverDataObj;
	}

	/**
	 * This method get the purchase detail from the server
	 * @param userId
	 * @return
	 */
	public String getPurchasedItemDetailByUserId(String userId)
	{
		String action = "getPurchasedItemsByUser";
		String strUrl = COMPLETE_URL  + "action=" + action + "&"
				+ "userId="   + userId +"&appId="+CommonUtils.APP_ID;

		//Log.e("MultiFriendzyServerOpr", " inside getPurchasedItemDetailByUserId url : " + strUrl);
		return this.parsePurchasedJsonString(CommonUtils.readFromURL(strUrl));
	}

	/**
	 * This method parse json String
	 * @param jsonString
	 * @return
	 */
	private String parsePurchasedJsonString(String jsonString)
	{
		if(ICommonUtils.LOG_ON)
			Log.e("MultiFriendzyServerOpr", 
					" inside parsePurchasedJsonString jsonString : " + jsonString);

		String result = "";

		try
		{
			JSONObject jObject = new JSONObject(jsonString);
			result = jObject.getString("data");			
			 if(MathFriendzyHelper.checkForKeyExistInJsonObj(jObject , "appsUnlock"))
				    result = result + "&" + jObject.getString("appsUnlock");
		}
		catch(JSONException e)
		{
			Log.e("MultiFriendzyServerOpr",
					"Error while parsing purchased item detail from server "  + e.toString());
		}
		return result;
	}

}

