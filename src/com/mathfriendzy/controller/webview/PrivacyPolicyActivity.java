package com.mathfriendzy.controller.webview;

import android.os.Bundle;
import android.webkit.WebView;

import com.mathfriendzy.controller.base.AdBaseActivity;
import com.firstgradepaid.R;

/**
 * This Activity open the given url in WebView
 * @author Yashwant Singh
 *
 */
public class PrivacyPolicyActivity extends AdBaseActivity 
{
	private WebView webView = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_privacy_policy);
		
		webView = (WebView) findViewById(R.id.privacypolicyWebView);
		String url = getIntent().getStringExtra("url");
		webView.loadUrl(url);
	}
}
