package com.mathfriendzy.model.result;

import static com.mathfriendzy.utils.ICommonUtils.FILE_PATH_ON_HOST;
import static com.mathfriendzy.utils.ICommonUtils.HOST_NAME;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;

import com.mathfriendzy.controller.result.ResultActivity;
import com.mathfriendzy.utils.CommonUtils;

public class JsonAsyncTaskForScore extends AsyncTask<Void, Void, String>
{
	private ProgressDialog pg = null;
	Context context;
	int userid;
	int playerid;
	String date;
	String playerName;
	boolean flag;
	
	
	public JsonAsyncTaskForScore(Context context, String date, int userid, 
			int playerid, String playerName, boolean flag){
		
		pg = CommonUtils.getProgressDialog(context);
		pg.show();
		
		this.context = context;
		this.userid = userid;
		this.playerid = playerid;
		this.date = date;	
		this.playerName	= playerName;
		this.flag		= flag;
	}

	
	private String getJsonFile()
	{
		String str = "";
		StringBuilder stringBuilder = new StringBuilder("");
		URL url = null;
		try {

			url = new URL(HOST_NAME + FILE_PATH_ON_HOST +
					"action=getMathsScore&userId="+userid+"&playerId="+playerid+"&date="+date+"&appId="+CommonUtils.APP_ID);
			//Log.e("", "url of score : "+url);
			BufferedReader ins = new BufferedReader(new InputStreamReader(url.openStream()));				
			try 
			{     
				while((str = ins.readLine()) != null)
				{				
					stringBuilder.append(str);
				}
			} 
			catch (IOException e) 
			{
				e.printStackTrace();
			}

		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return stringBuilder.toString();
	}

		
	@Override
	protected String doInBackground(Void... params)
	{		
		return getJsonFile();
	}

	
	@Override
	protected void onPostExecute(String result) 
	{
		pg.cancel();
		
		Intent intent = new Intent(context, ResultActivity.class);		
		intent.putExtra("userId", userid);
		intent.putExtra("playerId", playerid);
		intent.putExtra("playerName" , playerName);	
		intent.putExtra("date", date);
		intent.putExtra("jsonFile", result);	
		intent.putExtra("flag", flag);
		
		context.startActivity(intent);
		
		super.onPostExecute(result);
	}
}
