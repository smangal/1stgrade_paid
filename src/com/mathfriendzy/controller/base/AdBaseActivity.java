package com.mathfriendzy.controller.base;



import android.app.Activity;
import com.mathfriendzy.controller.ads.BannerAds;
import com.mathfriendzy.controller.ads.HouseAds;
import com.mathfriendzy.controller.ads.RateUsPops;

public class AdBaseActivity extends Activity  {

	protected boolean isHomeScreen = false;

	@Override
	protected void onResume() {
		BannerAds.getInstance(this).showAds();
		//HouseAds.getInstance().showAds(this);
		//HouseAds.getInstance().showCustomeAds(this);
		RateUsPops.getInstance(this).showRateUsPopUp();
		super.onResume();
	}
	
	/*protected void showHouseAd() {
		SharedPreferences pref = getSharedPreferences(ICommonUtils.ADS_FREQUENCIES_PREFF, 0);
		Editor editor = pref.edit();
		if(!pref.contains(ICommonUtils.ADS_FREQUENCIES_HOUSE_DATE)
				|| !pref.contains(ICommonUtils.ADS_timeIntervalFullAd)){
			HouseAds.getInstance().showAds(MyApplication.getAppContext());
			editor.putString(ICommonUtils.ADS_FREQUENCIES_HOUSE_DATE,
					CommonUtils.formateDate(new Date())).commit();
		}
		else if(CommonUtils.isTimeReachedToFrequencyTime(this)){
			HouseAds.getInstance().showAds(MyApplication.getAppContext());
			editor.putString(ICommonUtils.ADS_FREQUENCIES_HOUSE_DATE,
					CommonUtils.formateDate(new Date())).commit();
		}
	}*/
}
