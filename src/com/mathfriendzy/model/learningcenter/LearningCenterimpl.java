package com.mathfriendzy.model.learningcenter;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.mathfriendzy.database.Database;
import com.mathfriendzy.utils.AppVersion;
import com.mathfriendzy.utils.ICommonUtils;

public class LearningCenterimpl 
{	
	private SQLiteDatabase dbConn 	  	    = null;
	private Database database               = null;

	//table
	private String MATH_OPERATIONS 					= "trivia_operations";
	private String PLAYER_TOTAL_POINTS              = "PlayerTotalPoints";
	private String PLAYER_EQUATION_LEVEL            = "PlayerEquationLevel";
	private String PURCHASE_ITEMS                   = "PurchasedItems";

	//math result
	private String MATH_RESULT_TABLE                = "Math_Result";

	/**
	 * Constructor
	 * @param context
	 */
	public LearningCenterimpl(Context context)
	{
		database = new Database(context);
		database.open();		
	}

	/**
	 * This method open connection with database
	 */
	public void openConn()
	{
		if(dbConn == null)
			dbConn = database.getConnection();
	}

	/**
	 * This method close the database conneciton
	 */
	public void closeConn()
	{
		if(dbConn != null)
			dbConn.close();
	}

	/**
	 * This method return the learning center function from the database
	 * @return
	 */
	public ArrayList<LearningCenterTransferObj> getLearningCenterFunctions() 
	{
		ArrayList<LearningCenterTransferObj> learningCenterFunctions = new ArrayList<LearningCenterTransferObj>();
		LearningCenterTransferObj learningCenterObj = null;

		String query = "SELECT math_operation,math_operations_id FROM " + MATH_OPERATIONS ; 
		Cursor cursor = dbConn.rawQuery(query, null);

		while(cursor.moveToNext())
		{
			learningCenterObj = new LearningCenterTransferObj();
			learningCenterObj.setLearningCenterOperation(cursor.getString(0).replace("/", " "));
			learningCenterObj.setLearningCenterMathOperationId(cursor.getInt(1));
			learningCenterFunctions.add(learningCenterObj);
		}

		if(cursor != null)
			cursor.close();
		return learningCenterFunctions;
	}




	/**
	 * This method insert the data into Math Result Table
	 * @param mathResultTransferObj
	 */
	public void insertIntoMathResult(MathResultTransferObj mathResultTransferObj)
	{	
		ContentValues contentValues = new ContentValues();
		contentValues.put("gameType", mathResultTransferObj.getGameType());
		contentValues.put("userId", mathResultTransferObj.getUserId());
		contentValues.put("playerId", mathResultTransferObj.getPlayerId());
		contentValues.put("problems", mathResultTransferObj.getProblems());

		dbConn.insert(MATH_RESULT_TABLE, null, contentValues);
	}

	/**
	 * get record from Math_Result table
	 * @return
	 */
	public ArrayList<MathResultTransferObj> getMathResultData()
	{
		ArrayList<MathResultTransferObj> mathResultTransferObjList = new ArrayList<MathResultTransferObj>();
		String query = "select * from " + MATH_RESULT_TABLE;
		Cursor cursor = dbConn.rawQuery(query, null);

		while(cursor.moveToNext())
		{
			MathResultTransferObj mathobj = new MathResultTransferObj();
			//mathobj.setRoundId(cursor.getInt(cursor.getColumnIndex("roundId")));
			//mathobj.setIsFirstRound(cursor.getString(cursor.getColumnIndex("isFirstRound")));
			mathobj.setGameType(cursor.getString(cursor.getColumnIndex("gameType")));
			//mathobj.setIsWin(cursor.getInt(cursor.getColumnIndex("isWin")));
			mathobj.setUserId(cursor.getString(cursor.getColumnIndex("userId")));
			mathobj.setPlayerId(cursor.getString(cursor.getColumnIndex("playerId")));
			//mathobj.setIsFakePlayer(cursor.getInt(cursor.getColumnIndex("isFakePlayer")));
			mathobj.setProblems(cursor.getString(cursor.getColumnIndex("problems")));

			mathResultTransferObjList.add(mathobj);
		}

		if(cursor != null)
			cursor.close();

		return mathResultTransferObjList;
	}

	/**
	 * get record from Math_Result table
	 * @return
	 */
	public ArrayList<MathResultTransferObj> getMathResultData(String userId , String playerId)
	{
		ArrayList<MathResultTransferObj> mathResultTransferObjList = new ArrayList<MathResultTransferObj>();
		String query = "select * from " + MATH_RESULT_TABLE + " where userId = '" + userId 
				+ "' and playerId = '" + playerId + "'";

		Cursor cursor = dbConn.rawQuery(query, null);

		while(cursor.moveToNext())
		{
			MathResultTransferObj mathobj = new MathResultTransferObj();
			//mathobj.setRoundId(cursor.getInt(cursor.getColumnIndex("roundId")));
			//mathobj.setIsFirstRound(cursor.getString(cursor.getColumnIndex("isFirstRound")));
			mathobj.setGameType(cursor.getString(cursor.getColumnIndex("gameType")));
			//mathobj.setIsWin(cursor.getInt(cursor.getColumnIndex("isWin")));
			mathobj.setUserId(cursor.getString(cursor.getColumnIndex("userId")));
			mathobj.setPlayerId(cursor.getString(cursor.getColumnIndex("playerId")));
			//mathobj.setIsFakePlayer(cursor.getInt(cursor.getColumnIndex("isFakePlayer")));
			mathobj.setProblems(cursor.getString(cursor.getColumnIndex("problems")));

			mathResultTransferObjList.add(mathobj);
		}

		if(cursor != null)
			cursor.close();

		return mathResultTransferObjList;
	}

	/**
	 * This method insert into PlayerTotalPoints
	 * @param playerobj
	 */
	public void insertIntoPlayerTotalPoints(PlayerTotalPointsObj playerobj)
	{
		ContentValues contentValues = new ContentValues();
		contentValues.put("USER_ID", playerobj.getUserId());
		contentValues.put("PLAYER_ID", playerobj.getPlayerId());
		contentValues.put("TOTAL_POINTS", playerobj.getTotalPoints());
		contentValues.put("COINS", playerobj.getCoins());
		contentValues.put("COMPETE_LEVEL", playerobj.getCompleteLevel());
		contentValues.put("PURCHASED_COINS", playerobj.getPurchaseCoins());

		//Log.e("LearningCenterimpl", "competelevel " +playerobj.getCoins());
		dbConn.insert(PLAYER_TOTAL_POINTS, null, contentValues);
	}



	/**
	 * This method update the points of total player
	 * @param playerobj
	 */
	public void updatePlayerTotalPoints(PlayerTotalPointsObj playerobj)
	{
		String query = "update " + PLAYER_TOTAL_POINTS + " set TOTAL_POINTS = '" + playerobj.getTotalPoints() 
				+ " where PLAYER_ID = '" + playerobj.getPlayerId() +"'";

		dbConn.execSQL(query);


	}


	/**
	 * Update table for player equation playerId and user id
	 * @param userId
	 * @param playerId
	 */
	public void updatePlayerEquationTabelForUserIdAndPlayerId(String userId, String playerId)
	{
		//Log.e("Learnign center impl", "inside update fro player equation " + userId + " , " + playerId);

		/*String query = "update " + PLAYER_EQUATION_LEVEL + " set USER_ID = '" + userId 
						+ "' and PLAYER_ID = '" + playerId + "'" + "  where PLAYER_ID = '" + 0 +"' and " +
						" USER_ID = '" + 0 + "'";

		Log.e("LearningCenterImpl", "updatePlayerEquationTabelForUserIdAndPlayerId query" + query);
		dbConn.execSQL(query);*/

		String where = "USER_ID = '0' and PLAYER_ID = '0'";
		ContentValues cv = new ContentValues();
		cv.put("USER_ID", userId);
		cv.put("PLAYER_ID", playerId);

		dbConn.update(PLAYER_EQUATION_LEVEL, cv, where, null);
	}


	/**
	 * Update table for player equation playerId and user id
	 * @param userId
	 * @param playerId
	 */
	public void updateMathResultForUserIdAndPlayerId(String userId, String playerId)
	{
		//Log.e("Learnign center impl", "inside update fro player equation " + userId + " , " + playerId);
		/*String query = "update " + MATH_RESULT_TABLE + " set userId = '" + userId 
						+ "' and playerId = '" + playerId + "'" + "  where playerId = '" + 0 +"' and " +
						" userId = '" + 0 + "'";

		Log.e("LearningCenterImpl", "updateMathResultForUserIdAndPlayerId query" + query);
		dbConn.execSQL(query);*/

		String where = "userId = '0' and playerId = '0'";
		ContentValues cv = new ContentValues();
		cv.put("userId", userId);
		cv.put("playerId", playerId);

		dbConn.update(MATH_RESULT_TABLE, cv, where, null);
	}

	/**
	 * Update table for player total points id and user id
	 * @param userId
	 * @param playerId
	 */
	public void updatePlayerTotalPointsForUserIdandPlayerId(String userId, String playerId)
	{

		/*String query = "update " + PLAYER_TOTAL_POINTS + " set USER_ID = '" + userId 
				+ "' and PLAYER_ID = '" + playerId + "'" + "  where PLAYER_ID = '" + 0 +"' and " +
				" USER_ID = '" + 0 + "'";
		 */
		String where = "USER_ID = '0' and PLAYER_ID = '0'";
		ContentValues cv = new ContentValues();
		cv.put("USER_ID", userId);
		cv.put("PLAYER_ID", playerId);

		dbConn.update(PLAYER_TOTAL_POINTS, cv, where, null);

		/*Log.e("LearningCenterImpl", "updatePlayerTotalPointsForUserIdandPlayerId query" + query);
		dbConn.execSQL(query);*/
	}

	/**
	 * This method insert the data into the player Equation level table
	 * @param playrEquationObj
	 */
	public void insertIntoPlayerEquationLevel(PlayerEquationLevelObj playrEquationObj)
	{
		ContentValues contentValues = new ContentValues();
		contentValues.put("USER_ID", playrEquationObj.getUserId());
		contentValues.put("PLAYER_ID", playrEquationObj.getPlayerId());
		contentValues.put("EQUATION_TYPE", playrEquationObj.getEquationType());
		contentValues.put("LEVEL", playrEquationObj.getLevel());
		contentValues.put("STARS", playrEquationObj.getStars());	

		//Log.e("", "PLAYER_ID : PlayerEquationLevel : "+playrEquationObj.getPlayerId()+"  level "+playrEquationObj.getLevel());
		dbConn.insert(PLAYER_EQUATION_LEVEL, null, contentValues);
	}

	/**
	 * This method return true if it exist otherwise return false
	 * @param userid
	 * @param playerId
	 * @param level
	 * @param equationType
	 * @return
	 */
	public boolean isEquationPlayerExist(String userid,String playerId,int level,int equationType)
	{
		boolean isExist = false;
		String query = "select * from " + PLAYER_EQUATION_LEVEL + " where PLAYER_ID = '" +  playerId + "'"
				+ "and  USER_ID = '" + userid + "' and LEVEL = '" + level + "' and EQUATION_TYPE = '"
				+ equationType + "'";


		Cursor cursor = dbConn.rawQuery(query, null);

		if(cursor.moveToNext())
			isExist =  true;
		else 
			isExist =  false; 

		if(cursor != null)
			cursor.close();
		//Log.e("Learnign Center Impl ", "inseide isEquationplayerExist query " + query + " boolean value " + isExist);

		return isExist;
	}


	/**
	 * Update equation player level
	 * @param userid
	 * @param playerId
	 * @param level
	 * @param equationType
	 * @param stars
	 */
	public void updateEquationPlayer(String userid,String playerId,int level,int equationType, int stars)
	{
		String query = "update " + PLAYER_EQUATION_LEVEL + " set STARS = '" + stars +
				"' where PLAYER_ID = '" +  playerId + "'"
				+ "and  USER_ID = '" + userid + "' and LEVEL = '" + level + "' and EQUATION_TYPE = '"
				+ equationType + "'";

		dbConn.execSQL(query);
	}

	/**
	 * This method return the player level data
	 * @param playerId
	 * @return
	 */
	public ArrayList<PlayerEquationLevelObj> getPlayerEquationLevelDataByPlayerId(String playerId)
	{
		ArrayList<PlayerEquationLevelObj> playerDataList = new ArrayList<PlayerEquationLevelObj>();

		String query = "select * from " + PLAYER_EQUATION_LEVEL + " where PLAYER_ID = '" + playerId + "'";
		//Log.e("", "query "+query);
		Cursor cursor = dbConn.rawQuery(query, null);

		while(cursor.moveToNext())
		{
			PlayerEquationLevelObj playerObj = new PlayerEquationLevelObj();
			playerObj.setUserId(cursor.getString(cursor.getColumnIndex("USER_ID")));
			playerObj.setPlayerId(cursor.getString(cursor.getColumnIndex("PLAYER_ID")));
			playerObj.setEquationType(cursor.getInt(cursor.getColumnIndex("EQUATION_TYPE")));
			playerObj.setLevel(cursor.getInt(cursor.getColumnIndex("LEVEL")));
			playerObj.setStars(cursor.getInt(cursor.getColumnIndex("STARS")));				
			playerDataList.add(playerObj);	

		}

		if(cursor != null)
			cursor.close();
		return playerDataList;
	}


	/**
	 * This method return true if player exist otherwise return false
	 * @param playerId
	 * @return
	 */
	public boolean isPlayerRecordExits(String playerId)
	{
		boolean isExist = false;
		String query = "select * from " + PLAYER_TOTAL_POINTS + " where PLAYER_ID = " +  playerId;

		Cursor cursor = dbConn.rawQuery(query, null);

		if(cursor.moveToNext())
			isExist =  true;
		else 
			isExist =  false; 

		if(cursor != null)
			cursor.close();
		return isExist;
	}


	public void updatePlayerPoints(int points , String playerId)
	{
		//Log.e("LearningServerOpreration", "inside updatePlayerpoints");
		String query = "update " + PLAYER_TOTAL_POINTS + " set TOTAL_POINTS = '" + points +
				"' where PLAYER_ID = '" + playerId + "'";
		dbConn.execSQL(query);

		/*ContentValues contentValues = new ContentValues();
		contentValues.put("TOTAL_POINTS", points);
		dbConn.update(PLAYER_TOTAL_POINTS, contentValues, "PLAYER_ID", new String[]{playerId+""});*/
	}

	/**
	 * This methodb return the player points before playing it
	 * @param playerId
	 */
	public PlayerTotalPointsObj getDataFromPlayerTotalPoints(String playerId)
	{
		PlayerTotalPointsObj playerObj = new PlayerTotalPointsObj();
		String query = "select * from " + PLAYER_TOTAL_POINTS + " where PLAYER_ID = " +  playerId;
		Cursor cursor = dbConn.rawQuery(query, null);

		while(cursor.moveToNext())
		{
			playerObj.setPlayerId(cursor.getString(cursor.getColumnIndex("PLAYER_ID")));
			playerObj.setTotalPoints(cursor.getInt(cursor.getColumnIndex("TOTAL_POINTS")));
			playerObj.setCoins(cursor.getInt(cursor.getColumnIndex("COINS")));
			playerObj.setCompleteLevel(cursor.getInt(cursor.getColumnIndex("COMPETE_LEVEL")));
			playerObj.setPurchaseCoins(cursor.getInt(cursor.getColumnIndex("PURCHASED_COINS")));
		}
		//Log.e("", "coins are "+playerObj.getCoins());
		if(cursor != null)
			cursor.close();

		return playerObj;
	}

	/**
	 * This methodb return the player points before playing it
	 * @param playerId
	 */
	public PlayerTotalPointsObj getDataFromPlayerTotalPoints(String playerId , String userId)
	{
		PlayerTotalPointsObj playerObj = new PlayerTotalPointsObj();
		String query = "select * from " + PLAYER_TOTAL_POINTS + " where PLAYER_ID = '" +  playerId + "'"
				+ " and USER_ID = '" + userId + "'";
		Cursor cursor = dbConn.rawQuery(query, null);

		while(cursor.moveToNext())
		{
			playerObj.setPlayerId(cursor.getString(cursor.getColumnIndex("PLAYER_ID")));
			playerObj.setTotalPoints(cursor.getInt(cursor.getColumnIndex("TOTAL_POINTS")));
			playerObj.setCoins(cursor.getInt(cursor.getColumnIndex("COINS")));
			playerObj.setCompleteLevel(cursor.getInt(cursor.getColumnIndex("COMPETE_LEVEL")));
			playerObj.setPurchaseCoins(cursor.getInt(cursor.getColumnIndex("PURCHASED_COINS")));
		}

		if(cursor != null)
			cursor.close();

		return playerObj;
	}

	/**
	 * This method tells the existene of the player points in player total points
	 * @param playerId
	 * @return
	 */
	public boolean isPlayerTotalPointsExist(String playerId)
	{
		boolean isExist = false;
		String query = "select * from " + PLAYER_TOTAL_POINTS + " where PLAYER_ID = " +  playerId;
		Cursor cursor = dbConn.rawQuery(query, null);

		if(cursor.moveToNext())
			isExist = true;
		else
			isExist = false;

		if(cursor != null)
			cursor.close();
		return isExist;
	}

	/**
	 * This method insert the data into purchase table
	 * @param purchserItem
	 */
	public void insertIntoPurchaseItem(ArrayList<PurchaseItemObj> purchserItem)
	{
		for( int i = 0 ; i < purchserItem.size() ; i ++ )
		{			
			ContentValues contentValues = new ContentValues();
			contentValues.put("USER_ID", purchserItem.get(i).getUserId());
			contentValues.put("Item_id", purchserItem.get(i).getItemId());
			contentValues.put("Status", purchserItem.get(i).getStatus());
			dbConn.insert(PURCHASE_ITEMS, null, contentValues);
		}
	}

	/**
	 * This method update the player total points and complete level for single friendzy
	 * @param points
	 * @param completeLevel
	 * @param playerId
	 */
	public void updatePlayerPointsAndCmpleteLevel(int points , int completeLevel , String playerId)
	{	
		String where = "PLAYER_ID = '" + playerId + "'";
		ContentValues contentValues = new ContentValues();
		contentValues.put("TOTAL_POINTS", points);
		contentValues.put("COMPETE_LEVEL", completeLevel);

		dbConn.update(PLAYER_TOTAL_POINTS, contentValues, where, null);
	}

	/**
	 * This method delete the record from the player total points
	 */
	public void deleteFromPlayerTotalPoints()
	{
		dbConn.delete(PLAYER_TOTAL_POINTS , null , null);
	}


	/**
	 * this method return highestlevel with true for a equationtype
	 * @param playerEquationObj
	 * @return
	 */
	public boolean[] getHighestLevelForCheck(PlayerEquationLevelObj playerEquationObj)
	{
		boolean[] isCheck = new boolean[10];		
		int equ_type = 0;
		String query = "select EQUATION_TYPE from " + PLAYER_EQUATION_LEVEL + " where PLAYER_ID = '" +  playerEquationObj.getPlayerId() 
				+ "'" + "and  USER_ID = '" + playerEquationObj.getUserId()+"' and LEVEL == 10";
		Cursor cursor = dbConn.rawQuery(query, null);

		while(cursor.moveToNext())
		{			
			equ_type = cursor.getInt(0);

			isCheck[equ_type - 1] = true;

			//Log.e("", " check "+equ_type);
		}

		if(cursor != null)
			cursor.close();

		return isCheck;
	}


	/**
	 * this method return the hoghest level
	 * @param playerEquationObj
	 * @return
	 */
	public int getHighetsLevel(PlayerEquationLevelObj playerEquationObj)
	{
		int value = 0;
		String query = "select MAX(LEVEL) from " + PLAYER_EQUATION_LEVEL + " where PLAYER_ID = '" +  playerEquationObj.getPlayerId() 
				+ "'" + "and  USER_ID = '" + playerEquationObj.getUserId() + "' and EQUATION_TYPE = '" + playerEquationObj.getEquationType() + "'";
		Cursor cursor = dbConn.rawQuery(query, null);

		if(cursor.moveToNext())
			value =  cursor.getInt(0);

		if(cursor != null)
			cursor.close();

		return value;
	}


	/**
	 * This method return the purchase data from database
	 * @return
	 */
	public ArrayList<PurchaseItemObj>  getPurchaseItemData()
	{
		ArrayList<PurchaseItemObj> purchaseItemObj = new ArrayList<PurchaseItemObj>();

		String query = "select * from " + PURCHASE_ITEMS;
		Cursor cursor = dbConn.rawQuery(query, null);

		while(cursor.moveToNext())
		{
			PurchaseItemObj purchaseItem = new PurchaseItemObj();
			purchaseItem.setUserId(cursor.getString(cursor.getColumnIndex("USER_ID")));
			purchaseItem.setItemId(cursor.getInt(cursor.getColumnIndex("Item_id")));
			purchaseItem.setStatus(cursor.getInt(cursor.getColumnIndex("Status")));
			purchaseItemObj.add(purchaseItem);
		}

		return purchaseItemObj;
	}

	/**
	 * this method tell the status of the application
	 * @return
	 */
	@SuppressWarnings("unused")
	public int getAppUnlockStatus(int itemId , String userId)
	{
		//If there is some  change in this method then we need to do 
		//change in getAppUnlockStatusForResourcePurchase() also
		if(AppVersion.CURRENT_VERSION == AppVersion.PAID_VERSION){
			return 1;
		}
		
		int status = 0;
		String query = "select Status from " + PURCHASE_ITEMS + " where Item_id = '" + itemId + "'"
				+ " and USER_ID = '" + userId + "'";
		//Log.e("LearningCenterImpl", ""+query);
		Cursor cursor = dbConn.rawQuery(query, null);

		if(cursor.moveToNext())
			status = cursor.getInt(cursor.getColumnIndex("Status"));

		if(cursor != null)
			cursor.close();
		//Log.e("", ""+status);
		return status;
	}

	/**
	 * this method tell the status of the application
	 * @return
	 */
	@SuppressWarnings("unused")
	public int getAppUnlockStatusForResourcePurchase(int itemId , String userId)
	{	
		int status = 0;
		String query = "select Status from " + PURCHASE_ITEMS + " where Item_id = '" + itemId + "'"
				+ " and USER_ID = '" + userId + "'";
		//Log.e("LearningCenterImpl", ""+query);
		Cursor cursor = dbConn.rawQuery(query, null);

		if(cursor.moveToNext())
			status = cursor.getInt(cursor.getColumnIndex("Status"));

		if(cursor != null)
			cursor.close();
		//Log.e("", ""+status);
		return status;
	}

	/**
	 * This method delete the record from Math_result Table
	 * @param playerId
	 */
	public void deleteFromMathResult()
	{
		dbConn.delete(MATH_RESULT_TABLE , null , null);
	}

	/**
	 * This method delete the record from Math_result Table
	 * @param playerId
	 */
	public void deleteFromMathResult(String userid , String playerid)
	{
		String where = "userId = '"+ userid + "' and playerId = '" + playerid + "'";
		dbConn.delete(MATH_RESULT_TABLE , where , null);
	}


	/**
	 * This method delete the record from the player total pints based on player id
	 * @param playerId
	 */
	public void deleteFromPurchaseItem(String userId)
	{
		dbConn.delete(PURCHASE_ITEMS , "User_id=?" , new String[]{userId + ""});
	}

	/**
	 * This method delete the record from the player total pints based on player id
	 * @param
	 */
	public void deleteFromPurchaseItem(String userId , int itemId)
	{
		String where = "User_id = '"+ userId + "' and Item_id = '" + itemId + "'";
		dbConn.delete(PURCHASE_ITEMS , where , null);
	}
	
	/**
	 * This method delete the record from the player total pints based on player id
	 * @param playerId
	 */
	public void deleteFromPlayerTotalPoints(String playerId)
	{
		//Log.e("Learnig Center Impl ", " delete from total points");
		dbConn.delete(PLAYER_TOTAL_POINTS , "PLAYER_ID = ?" , new String[]{playerId});
	}


	/**
	 * This method delete the record from the player total points
	 * Except the temp player record if exist
	 * 
	 *  This method call when a user login , This method delete the record which already exist
	 *  and save current record ,  If temp player exist the delete all record except tepm player record
	 * @param playerId
	 */
	public void deleteFromPlayerTotalPointsExceptTempPlayerData()
	{
		//Log.e("Learnig Center Impl ", " delete from total points");
		dbConn.delete(PLAYER_TOTAL_POINTS , "PLAYER_ID != ? and USER_ID != ?" , new String[]{"0","0"});
	}

	/**
	 * This method delete the record from the player total pints based on player id
	 * @param playerId
	 */
	public void deleteFromPlayerEruationLevel(String playerId)
	{
		dbConn.delete(PLAYER_EQUATION_LEVEL , "PLAYER_ID = ?" , new String[]{playerId});
	}

	/**
	 * This method delete the record from Math_result Table
	 * @param playerId
	 */
	public void deleteFromPlayerEruationLevel()
	{
		dbConn.delete(PLAYER_EQUATION_LEVEL , null , null);
	}

	/**
	 * This method delete the record from the player total pints based on player id
	 * @param playerId
	 */
	public void deleteFromPurchaseItem()
	{
		dbConn.delete(PURCHASE_ITEMS , null , null);
	}

	/**
	 * This method delete the record from the player total pints based on player id
	 * @param playerId
	 */
	public void deleteFromPurchaseItem(int itemId)
	{
		dbConn.delete(PURCHASE_ITEMS , "Item_id=?" , new String[]{itemId + ""});
	}


	/**
	 * This method update the player coins
	 * @param coins
	 * @param userId
	 * @param playerId
	 */
	public void updateCoinsForPlayer(int coins , String userId , String playerId)
	{
		String where = "USER_ID = '"+ userId + "' and PLAYER_ID = '" + playerId + "'";
		ContentValues cv = new ContentValues();
		cv.put("COINS", coins);

		dbConn.update(PLAYER_TOTAL_POINTS, cv, where, null);
	}

	/**
	 * This method return the purchase data from database
	 * @return
	 */
	public ArrayList<String>  getPurchaseItemIdsByUserId(String userId)
	{
		ArrayList<String> purchaseItemObj = new ArrayList<String>();

		String query = "select Item_id from " + PURCHASE_ITEMS + " where User_id = '" + userId + "'";
		Cursor cursor = dbConn.rawQuery(query, null);

		while(cursor.moveToNext())
		{
			purchaseItemObj.add(cursor.getString(cursor.getColumnIndex("Item_id")));
		}

		if(cursor != null)
			cursor.close();

		return purchaseItemObj;
	}


	/**
	 * Update the purchased item for temp player if it become a login user player
	 * @param userId
	 */
	public void updatePurchasedItemTable(String userId)
	{	
		String where = "User_id = '0'";
		ContentValues cv = new ContentValues();
		cv.put("User_id", userId);
		dbConn.update(PURCHASE_ITEMS, cv, where, null);
	}



}
