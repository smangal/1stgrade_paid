package com.mathfriendzy.controller.player;

import static com.mathfriendzy.utils.ICommonUtils.ADD_TEMP_PLAYER_STEP1_FLAG;
import static com.mathfriendzy.utils.ICommonUtils.IS_FACEBOOK_LOGIN;
import static com.mathfriendzy.utils.ICommonUtils.IS_LOGIN_FROM_FACEBOOK;

import java.util.ArrayList;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.firstgradepaid.R;
import com.mathfriendzy.controller.chooseavtar.ChooseAvtars;
import com.mathfriendzy.facebookconnect.FacebookConnect;
import com.mathfriendzy.model.country.Country;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.model.states.States;
import com.mathfriendzy.utils.CommonUtils;
import com.mathfriendzy.utils.DialogGenerator;
import com.mathfriendzy.utils.Validation;

/**
 * Add temp player setp 1
 * @author Yashwant Singh
 *
 */
public class AddTempPlayerStep1Activity extends BasePlayer implements OnClickListener
{
	private Button btnTitleSubmit 	 = null;
	private TextView lblTransferYourInfoFromFacebookAndFindFriends = null;
	
	public static String countryName = null;
	public static String stateName   = null;
	public static String city		 = null;
	public static String zipCode	 = null;
	public static String firstName   = null;
	public static String lastName    = null;
	public static String userName    = null;
	
	private final String IMAGE_NAME                 = "Smiley";
	private String TAG = this.getClass().getSimpleName();
	
	ProgressDialog progressDialog = null;
	private Button   btnTitleBack = null;
	
	private SharedPreferences facebookPreference= null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_temp_player_step1);
		
		if(ADD_TEMP_PLAYER_STEP1_FLAG)
			Log.e(TAG, "inside onCreate()");
		
		facebookPreference = getSharedPreferences(IS_FACEBOOK_LOGIN, 0);
		
		this.setWidgetsReferences();
		this.setWidgetsTextValues();
		this.getCountries(this,"United States");
		this.getBitmap();
		this.setListenerOnWidgets();
		
		if(ADD_TEMP_PLAYER_STEP1_FLAG)
			Log.e(TAG, "outside onCreate()");
	}
	
	/**
	 * This method get bitmap from image to store into database
	 */
	private void getBitmap()
	{
		if(ADD_TEMP_PLAYER_STEP1_FLAG)
			Log.e(TAG, "inside getBitmap()");
		
		imageName = IMAGE_NAME;
		Resources res = this.getResources();
		Drawable drawable  = res.getDrawable(R.drawable.smiley);
		profileImageBitmap = ((BitmapDrawable)drawable).getBitmap();
		
		if(ADD_TEMP_PLAYER_STEP1_FLAG)
			Log.e(TAG, "outside getBitmap()");
	}

	/**
	 * This methos set the widhets references from layout to the widgets object
	 */
	private void setWidgetsReferences()
	{
		if(ADD_TEMP_PLAYER_STEP1_FLAG)
			Log.e(TAG, "inside setWidgetsReferences()");
		
		edtFirstName 	= (EditText) findViewById(R.id.edtFirstName);
		edtLastName  	= (EditText) findViewById(R.id.edtLastName);
		edtUserName  	= (EditText) findViewById(R.id.edtUserName);
		edtCity 	 	= (EditText) findViewById(R.id.edtCity);
		edtZipCode 	 	= (EditText) findViewById(R.id.edtZipCode);
		spinnerCountry	= (Spinner) findViewById(R.id.spinnerCountry);
		spinnerState 	= (Spinner) findViewById(R.id.spinnerState);
		imgAvtar        = (ImageView) findViewById(R.id.imgAvtar);
		btnTitleSubmit  = (Button) findViewById(R.id.btnTitleSubmit);
				
		mfTitleHomeScreen 	= (TextView) findViewById(R.id.mfTitleHomeScreen);
		lblEditPlayerTitle 	= (TextView) findViewById(R.id.lblEditPlayerTitle);
		lblFbConnect 		= (Button) findViewById(R.id.lblFbConnect);
		lblFirstName 		= (TextView) findViewById(R.id.lblFirstName);
		lblLastName 		= (TextView) findViewById(R.id.lblLastName);
		lblUserName 		= (TextView) findViewById(R.id.lblUserName);
		lblRegCountry 		= (TextView) findViewById(R.id.lblRegCountry);
		lblRegCity 			= (TextView) findViewById(R.id.lblRegCity);
		lblRegZip 			= (TextView) findViewById(R.id.lblRegZip);
		lblRegState			= (TextView) findViewById(R.id.lblRegState);
		lblChooseAnAvatar 	= (TextView) findViewById(R.id.lblChooseAnAvatar);
		avtarLayout         = (RelativeLayout) findViewById(R.id.avtarLayout);
		btnTitleBack        = (Button)   findViewById(R.id.btnTitleBack);
		
		lblTransferYourInfoFromFacebookAndFindFriends = (TextView) findViewById(R.id.EditPlayerTxtTransferInfo);
		
		if(ADD_TEMP_PLAYER_STEP1_FLAG)
			Log.e(TAG, "outside setWidgetsReferences()");
	}
	
	/**
	 * this method set listener on widgets
	 */
	private void setListenerOnWidgets() 
	{
		if(ADD_TEMP_PLAYER_STEP1_FLAG)
			Log.e(TAG, "inside setListenerOnWidgets()");
		
		spinnerCountry.setOnItemSelectedListener(new OnItemSelectedListener() 
		{
			@Override
			public void onItemSelected(AdapterView<?> arg0, View view,int arg2, long arg3) 
			{				
				if(spinnerCountry.getSelectedItem().toString().equals("United States"))
				{
					spinnerState.setVisibility(Spinner.VISIBLE);
					lblRegState.setVisibility(TextView.VISIBLE);
					edtZipCode.setEnabled(true);
					lblRegZip.setTextColor(Color.BLACK);
					setStates("United States");
				}
				else if(spinnerCountry.getSelectedItem().toString().equals("Canada"))
				{
					spinnerState.setVisibility(Spinner.VISIBLE);
					lblRegState.setVisibility(TextView.VISIBLE);
					edtZipCode.setEnabled(true);
					lblRegZip.setTextColor(Color.BLACK);
					setStates("Canada");
				}
				else
				{
					spinnerState.setVisibility(Spinner.GONE);
					lblRegState.setVisibility(TextView.GONE);
					edtZipCode.setText("");
					lblRegZip.setTextColor(Color.GRAY);
					edtZipCode.setEnabled(false);
					
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) 
			{				
			}
		});
		
		btnTitleSubmit.setOnClickListener(this);
		avtarLayout.setOnClickListener(this);
		lblFbConnect.setOnClickListener(this);
		btnTitleBack.setOnClickListener(this);
				
		if(ADD_TEMP_PLAYER_STEP1_FLAG)
			Log.e(TAG, "outside setListenerOnWidgets()");
	}
	
	
	/**
	 * This method check the user is valid or not
	 * @param userName
	 */
	private void checkValidUser(String userName)
	{
		//Changes For Internet Connection
		if(CommonUtils.isInternetConnectionAvailable(this))
		{
			progressDialog = CommonUtils.getProgressDialog(AddTempPlayerStep1Activity.this);
			new CkeckValidUserAsyncTask(userName).execute(null,null,null);
			//execute Asynctask for check user is already registered or not
		}
		else
		{
			DialogGenerator dg = new DialogGenerator(this);
			Translation transeletion = new Translation(this);
			transeletion.openConnection();
			dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgYouAreNotConnectedToTheInternet"));
			transeletion.closeConnection();
		}
		
	}
	
	/**
	 * This method check for valid city
	 */
	private void checkValidCity()
	{
		if(spinnerCountry.getSelectedItem().toString().equals("United States") || spinnerCountry.getSelectedItem().toString().equals("Canada"))
		{
			Country countryObj = new Country();
			String countryId = countryObj.getCountryIdByCountryName(spinnerCountry.getSelectedItem().toString(), this);
			
			States stateObj = new States();
			String stateId  = null;
			
			if(spinnerCountry.getSelectedItem().toString().equals("Canada"))
				 stateId  = stateObj.getCanadaStateIdByName(spinnerState.getSelectedItem().toString(), this);
			else if(spinnerCountry.getSelectedItem().toString().equals("United States"))
				 stateId  = stateObj.getUSStateIdByName(spinnerState.getSelectedItem().toString(), this);
			
			//changes for Internet Connection
			if(CommonUtils.isInternetConnectionAvailable(this))
			{
				new CheckValidCityAsynTask(countryId,stateId,edtCity.getText().toString(), edtZipCode.getText().toString()).execute(null,null,null);
				//execute Asynctask for check city is city and zip valid or not
			}
			else
			{
				DialogGenerator dg = new DialogGenerator(this);
				Translation transeletion = new Translation(this);
				transeletion.openConnection();
				dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgYouAreNotConnectedToTheInternet"));
				transeletion.closeConnection();
			}
			
		}
		else
		{
			progressDialog.cancel();
			Intent intent = new Intent(AddTempPlayerStep1Activity.this,AddTempPlayerStep2Activity.class);
			startActivity(intent);
		}
	}
		
	@Override
	public void onClick(View v) 
	{
		switch(v.getId())
		{
		case R.id.btnTitleSubmit:
			this.getDataFromWidgets();
			break;
		case R.id.avtarLayout :
			Intent chooseAvtar  = new Intent(this,ChooseAvtars.class);
			startActivity(chooseAvtar);
			break;
		case R.id.lblFbConnect :
			this.facebookConnect();
			//changes for Internet Connection
			/*if(CommonUtils.isInternetConnectionAvailable(this))
			{
				Intent intentFacebook = new Intent(this,FacebookConnect.class);
				startActivity(intentFacebook);
			}
			else
			{
				DialogGenerator dg = new DialogGenerator(this);
				Translation transeletion = new Translation(this);
				transeletion.openConnection();
				dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgYouAreNotConnectedToTheInternet"));
				transeletion.closeConnection();
			}*/
			break;
		case R.id.btnTitleBack:
			Intent intentCreate = new Intent(this,CreateTempPlayerActivity.class);
			startActivity(intentCreate);
			break;
		}
	}
	
	
	/**
	 * This method connect with facebook and if already connect to facebook then disconnect from facebook
	 */
	private void facebookConnect()
	{
		Translation transeletion1 = new Translation(this);
		transeletion1.openConnection();		
		
		SharedPreferences.Editor editor = facebookPreference.edit();
		
		if(facebookPreference.getBoolean(IS_LOGIN_FROM_FACEBOOK, false))
		{
			FacebookConnect.logoutFromFacebook();
			editor.putBoolean(IS_LOGIN_FROM_FACEBOOK, false);
			editor.commit();
			lblFbConnect.setText(transeletion1.getTranselationTextByTextIdentifier("lblFbConnect"));
			lblTransferYourInfoFromFacebookAndFindFriends.setText(transeletion1.getTranselationTextByTextIdentifier
					("lblTransferYourInfoFromFacebookAndFindFriends"));
		}
		else
		{			
			//changes for Internet Connection
			if(CommonUtils.isInternetConnectionAvailable(this))
			{					
				Intent intent = new Intent(this,FacebookConnect.class);
				startActivity(intent);
				editor.putBoolean(IS_LOGIN_FROM_FACEBOOK, true);
				editor.commit();
				lblFbConnect.setText(transeletion1.getTranselationTextByTextIdentifier("btnTitleLogOut"));
				lblTransferYourInfoFromFacebookAndFindFriends.setText(transeletion1.getTranselationTextByTextIdentifier
						("lblDisconnectFromFb"));
			}
			else
			{
				DialogGenerator dg1 = new DialogGenerator(this);
				dg1.generateWarningDialog(transeletion1.getTranselationTextByTextIdentifier("alertMsgYouAreNotConnectedToTheInternet"));
			}
		}
		transeletion1.closeConnection();
	}
	
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) 
	{
		if(keyCode == KeyEvent.KEYCODE_BACK)
		{
			Intent intentCreate = new Intent(this,CreateTempPlayerActivity.class);
			startActivity(intentCreate);
			return false;
		
		}return super.onKeyDown(keyCode, event);
	}
	
	/**
	 * This method get tha data from the widgets which is set by the user and store it
	 */
	private void getDataFromWidgets()
	{
		 countryName  = spinnerCountry.getSelectedItem().toString();
			if(spinnerState.getSelectedItem() != null)
				stateName = spinnerState.getSelectedItem().toString();
			
			if(spinnerCountry.getSelectedItem().toString().equals("United States") || spinnerCountry.getSelectedItem().toString().equals("Canada"))
			{
				if(edtCity.getText().toString().equals("") || edtZipCode.getText().toString().equals("") 
						|| edtFirstName.getText().toString().equals("") || edtLastName.getText().toString().equals(""))
				{
					this.generateWarningDailog();
				}
				else
				{
					this.setData();
				}
			}
			else
			{
				if(edtCity.getText().toString().equals("")  
						|| edtFirstName.getText().toString().equals("") || edtLastName.getText().toString().equals(""))
				{
					
					this.generateWarningDailog();
				}
				else
				{
					this.setData();
				}
			}
	}
	
	/**
	 * This methos generates warning dialog
	 */
	private void generateWarningDailog()
	{
		Translation transeletion = new Translation(this);
		transeletion.openConnection();
		DialogGenerator dg = new DialogGenerator(this);
		dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgPleaseEnterAllInfo"));
		transeletion.closeConnection();
	}
	
	/**
	 * This methos set the data from widgets to the static variable
	 */
	private void setData()
	{
		city      = edtCity.getText().toString();
		zipCode   = edtZipCode.getText().toString();
		firstName = edtFirstName.getText().toString();
		lastName  = edtLastName.getText().toString();
		checkValidUser(edtUserName.getText().toString());
	}
	
	@Override
	protected void onRestart() 
	{
		this.setTextFromFacebook();
		super.onRestart();
	}
		
	/**
	 * This methos set the text values from the transelation
	 */
	private void setWidgetsTextValues()
	{
		if(ADD_TEMP_PLAYER_STEP1_FLAG)
			Log.e(TAG, "inside setWidgetsTextValues()");
		
		Translation transeletion = new Translation(this);
		transeletion.openConnection();
		mfTitleHomeScreen.setText(transeletion.getTranselationTextByTextIdentifier("homeTitleFriendzy"));
		lblEditPlayerTitle.setText(transeletion.getTranselationTextByTextIdentifier("lblCreatePlayer") + " :");
		lblFirstName.setText(transeletion.getTranselationTextByTextIdentifier("lblFirstName") + " :");
		lblLastName.setText(transeletion.getTranselationTextByTextIdentifier("lblLastName") + " :");
		lblUserName.setText(transeletion.getTranselationTextByTextIdentifier("lblUserName") + " :");
		lblRegCountry.setText(transeletion.getTranselationTextByTextIdentifier("lblRegCountry") + " :");
		lblRegState.setText(transeletion.getTranselationTextByTextIdentifier("lblRegState") + " :");
		lblRegCity.setText(transeletion.getTranselationTextByTextIdentifier("lblRegCity") + " :");
		lblRegZip.setText(transeletion.getTranselationTextByTextIdentifier("lblRegZip") + " :");
		lblChooseAnAvatar.setText(transeletion.getTranselationTextByTextIdentifier("lblChooseAnAvatar"));
		btnTitleSubmit.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleSubmit"));
		btnTitleBack.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleBack"));
		
		
		if(!facebookPreference.getBoolean(IS_LOGIN_FROM_FACEBOOK, false))
		{
			lblFbConnect.setText(transeletion.getTranselationTextByTextIdentifier("lblFbConnect"));
			lblTransferYourInfoFromFacebookAndFindFriends.setText(transeletion.getTranselationTextByTextIdentifier
						("lblTransferYourInfoFromFacebookAndFindFriends"));
		}
		else
		{
			lblFbConnect.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleLogOut"));
			lblTransferYourInfoFromFacebookAndFindFriends.setText(transeletion.getTranselationTextByTextIdentifier
					("lblDisconnectFromFb"));
		}
		
		transeletion.closeConnection();
		
		if(ADD_TEMP_PLAYER_STEP1_FLAG)
			Log.e(TAG, "outside setWidgetsTextValues");
	}
	
	/**
	 * This methos set states when user select country either united state or Canada
	 * @param countryName
	 */
	private void setStates(String countryName)
	{
		if(ADD_TEMP_PLAYER_STEP1_FLAG)
			Log.e(TAG, "inside setStates()");
		
		ArrayList<String> satateList = new ArrayList<String>();
		States stateObj = new States();
		if(countryName.equals("United States"))
			satateList = stateObj.getUSStates(this);
		else if(countryName.equals("Canada"))
			satateList = stateObj.getCanadaStates(this);
		
		ArrayAdapter<String> stateAdapter = new ArrayAdapter<String>(AddTempPlayerStep1Activity.this, android.R.layout.simple_list_item_1,satateList);
		stateAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinnerState.setAdapter(stateAdapter);
		stateAdapter.notifyDataSetChanged();
		
		if(ADD_TEMP_PLAYER_STEP1_FLAG)
			Log.e(TAG, "outside setStates()");
	}
	
	/**
	 * @descrition check user on server valid or not
	 * @author Yashwant Singh
	 *
	 */
	class CkeckValidUserAsyncTask extends AsyncTask<Void, Void, Void>
	{
		private String userName = null;
		private String resultValue   = null;
		
		public CkeckValidUserAsyncTask(String userName)
		{
			this.userName = userName;
		}
		
		@Override
		protected void onPreExecute()
		{
			progressDialog.show();
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params) 
		{
			Validation validObj = new Validation();
			resultValue = validObj.validUser(userName);
			return null;
		}

		@Override
		protected void onPostExecute(Void result) 
		{
			if(resultValue.equals("0"))//O for Valid User Name
			{
				AddTempPlayerStep1Activity.userName  = edtUserName.getText().toString();
				checkValidCity();
			}
			else if(resultValue.equals("1"))//1 for user name already exist
			{
				progressDialog.cancel();
				
				Translation transeletion = new Translation(AddTempPlayerStep1Activity.this);
				transeletion.openConnection();
				DialogGenerator dg = new DialogGenerator(AddTempPlayerStep1Activity.this);
				dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgThisUserNameAlreadyExist"));
				transeletion.closeConnection();	
			}
			super.onPostExecute(result);
		}
	}
	
	/**
	 * @Description check for city is valid or not
	 * @author Yashwant Singh
	 *
	 */
	class CheckValidCityAsynTask extends AsyncTask<Void, Void, Void>
	{
		String countryId 	= null;
		String stateId 		= null;
		String city 		= null;
		String zip 			= null;
		String resultValue  = null;

		CheckValidCityAsynTask(String countryId,String stateId,String city,String zip)
		{
			this.countryId = countryId;
			this.stateId   = stateId;
			this.city      = city;
			this.zip       = zip;
		}
		
		@Override
		protected void onPreExecute() 
		{
			super.onPreExecute();
		}
		
		@Override
		protected Void doInBackground(Void... params) 
		{
			Validation validObj = new Validation();
			resultValue = validObj.validateCity(countryId, stateId, city, zip);
			return null;
		}

		@Override
		protected void onPostExecute(Void result) 
		{
			progressDialog.cancel();
			
			Translation transeletion = new Translation(AddTempPlayerStep1Activity.this);
			transeletion.openConnection();
			
			if(resultValue.equals("-9001"))//for invalid City
			{
				DialogGenerator dg = new DialogGenerator(AddTempPlayerStep1Activity.this);
				dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgInvalidCity"));
			}
			else if(resultValue.equals("-9002"))//for invalid ZipCode
			{
				DialogGenerator dg = new DialogGenerator(AddTempPlayerStep1Activity.this);
				dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgInvalidZipCode"));
			}
			else
			{
				Intent intent = new Intent(AddTempPlayerStep1Activity.this,AddTempPlayerStep2Activity.class);
				startActivity(intent);
			}
			transeletion.closeConnection();	
			
			super.onPostExecute(result);
		}
	}
}
