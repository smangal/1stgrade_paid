package com.mathfriendzy.notification;

import com.mathfriendzy.utils.CommonUtils;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;
import static com.mathfriendzy.utils.ICommonUtils.CONPLETE_URL_FOR_NOTIFICATION;
import static com.mathfriendzy.utils.ICommonUtils.DEVICE_ID;
import static com.mathfriendzy.utils.ICommonUtils.DEVICE_ID_PREFF;

/**
 * This class add user on server with android device
 * @author Yashwant Singh
 *
 */
public class AddUserWithAndroidDevice extends AsyncTask<Void, Void, Void> 
{
	private String userId;
	private String udid;
	
	public AddUserWithAndroidDevice(String userId , Context context)
	{
		
		SharedPreferences sharedPreff = context.getSharedPreferences(DEVICE_ID_PREFF, 0);
		this.udid = sharedPreff.getString(DEVICE_ID, "");
		this.userId = userId;
	}
	
	@Override
	protected Void doInBackground(Void... params) 
	{		
		addUserWithAndroidDevice(userId , udid);
		return null;
	}
	
	/**
	 * This method add user on server
	 * @param userId
	 * @param udid
	 */
	private void addUserWithAndroidDevice(String userId , String udid)
	{
		String strUrl = CONPLETE_URL_FOR_NOTIFICATION 
					 + "action=addUserWithAndroidDevice" + "&" 
					 + "userId=" + userId + "&"
					 + "udid="+ udid +"&appId=" + CommonUtils.APP_ID;
	
		//Log.e("AddUserWithAndroidDevice", "inside addUserWithAndroidDevice url : " +strUrl);
		this.parseJsonForaddUserWithAndroidDevice(CommonUtils.readFromURL(strUrl));
	}
	
	/**
	 * This method parseJson String
	 * @param jsonString
	 */
	private void parseJsonForaddUserWithAndroidDevice(String jsonString)
	{
		//Log.e("", "response :" + jsonString);
	}
}
