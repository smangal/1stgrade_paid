package com.mathfriendzy.controller.ads;

import java.util.Date;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;

import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.utils.CommonUtils;
import com.mathfriendzy.utils.ICommonUtils;

/**
 * This asyncktask get the ads frequesncy from the server
 * @author Yashwant Singh
 *
 */
public class GetAdsFrequencies extends AsyncTask<Void, Void, AddFrequency>{

	private Context context = null;

	public GetAdsFrequencies(Context context){
		this.context = context;
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
	}

	@Override
	protected AddFrequency doInBackground(Void... params) {
		AdsServerOperation adsServerOperation = new AdsServerOperation();
		return adsServerOperation.getFrequesncies();
	}

	@Override
	protected void onPostExecute(AddFrequency frequencies){
		if(frequencies != null){
			SharedPreferences sharedPreff = context.
					getSharedPreferences(ICommonUtils.ADS_FREQUENCIES_PREFF, 0);
			SharedPreferences.Editor editor = sharedPreff.edit();
			editor.putInt(ICommonUtils.ADS_FREQUENCIES, frequencies.getData());
			editor.putString(ICommonUtils.ADS_FREQUENCIES_DATE,
					CommonUtils.formateDate(new Date()));
			editor.putInt(ICommonUtils.ADS_timeIntervalFullAd ,
					frequencies.getTimeIntervalFullAd());
			editor.putInt(ICommonUtils.ADS_timeIntervalFullAdForPaid ,
					frequencies.getTimeIntervalFullAdForPaid());						
			editor.commit();
			
			//for new rate us dialog and house ads display based on frequency
			MathFriendzyHelper.saveIntegerValue(context, MathFriendzyHelper.HOUSE_ADS_FREQUENCY_KEY,
					frequencies.getHouseAdsFrequemcy());
			MathFriendzyHelper.saveIntegerValue(context, MathFriendzyHelper.RATE_ADS_FREQUENCY_KEY,
					frequencies.getRatePopUpFrequency());		
			MathFriendzyHelper.saveStringValueToPreff(context, MathFriendzyHelper.HOUSE_ADS_IMAGE_NAME, 
					frequencies.getHouseAdImageName());
			MathFriendzyHelper.saveStringValueToPreff(context, MathFriendzyHelper.HOUSE_ADS_LINK_URL, 
					frequencies.getHoudeAdAndroidUrl());
			//MathFriendzyHelper.removeHouseAdsImageFromCache(context);
		}		
		super.onPostExecute(frequencies);
	}
}

