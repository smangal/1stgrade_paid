package com.mathfriendzy.utils;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.annotation.SuppressLint;
import android.net.ParseException;
import android.util.Log;

public class DateTimeOperation
{
	public String setTimeFormat(long startTime)
	{   	
		String space = "";
		String space2 = "";
		String space3 = "";

		long hour = ((startTime)/(60*60));
		long min = ((startTime)/(60)- (hour)*60);
		long sec = (startTime - (min + hour*60)*60);

		if(hour < 10)
		{
			space = "0";
		}
		if(min < 10)
		{
			space2 = "0";					
		}
		if(sec < 10)
		{
			space3 = "0";
		}

		return space+hour+":"+space2+min+":"+space3+sec;
	}//END setTimeFormat method


	public static String setTimeInMinAndSec(long startTime)
	{   	
		String space = "";
		String space2 = "";

		long min = (startTime / 60);
		long sec = (startTime - (min * 60));

		if(min < 10)
		{
			space = "0";					
		}
		if(sec < 10)
		{
			space2 = "0";
		}
		//Log.e("startTime",""+startTime);
		return space + min + ":" + space2+sec;
	}//END setTimeFormat method


	public String setNumberString(String str)
	{
		try{
			double amount = Double.parseDouble(str);
			DecimalFormat formatter = new DecimalFormat("###,###,###,###");
			return formatter.format(amount);
		}catch(Exception e){
			return "0";
		}
	}

	@SuppressLint("SimpleDateFormat")
	@SuppressWarnings("deprecation")
	public String setDate(String strdate) 
	{
		Date date = null;
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		try {
			date = formatter.parse(strdate);
		} catch (ParseException e) {
			e.printStackTrace();
		} catch (java.text.ParseException e) {
			e.printStackTrace();
		}
		String month = getMonthInWord(date.getMonth());		
		String finalDate = date.getDate()+ " "+month+" "+(date.getYear()+1900);

		return finalDate;
	}

	public String getMonthInWord(int i)
	{
		String month = "";
		switch(i)
		{
		case 0:
			month = "January";
			break;
		case 1:
			month = "February";
			break;
		case 2:
			month = "March";
			break;
		case 3:
			month = "April";
			break;
		case 4:
			month = "May";
			break;
		case 5:
			month = "June";
			break;
		case 6:
			month = "July";
			break;
		case 7:
			month = "August";
			break;
		case 8:
			month = "September";
			break;
		case 9:
			month = "October";
			break;
		case 10:
			month = "November";
			break;
		case 11:
			month = "December";
		}
		return month;
	}


	public static long getDuration(String strdate)
	{
		Date date = null;
		Date currentDate = new Date();

		SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
		SimpleDateFormat fm = new SimpleDateFormat("dd MMM yyyy kk:mm:ss 'GMT'");
		try {
			date = formatter.parse(strdate);			
			currentDate = fm.parse(currentDate.toGMTString());

			//currentDate = formatter.parse(formatter.format(calender.getTime()));	
		} catch (ParseException e) {
			e.printStackTrace();
		} catch (java.text.ParseException e) {
			e.printStackTrace();
		}

		//Log.e("Date", ""+date+", currentdate "+currentDate.toGMTString());
		if(date == currentDate)
		{
			return 0;
		}

		//return Math.max(currentDate.getTime(), date.getTime()) - Math.min(currentDate.getTime(), date.getTime());
		return date.getTime()- currentDate.getTime();
	}


	@SuppressWarnings("deprecation")
	public static String getEndTimeAfterAddedExtraTime(String endDate , int secondsToBeAddedd){

		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss");
		try {
			Date endDateFromString   = df.parse(endDate);
			endDateFromString.setSeconds(endDateFromString.getSeconds() + secondsToBeAddedd);
			return df.format(endDateFromString);

		}catch (java.text.ParseException e) {
			e.printStackTrace();
		}
		return endDate;
	}

	public static boolean isValidForChange(Date endDateFromString, Date currentDate){

		//SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss");

		//try {
		//Date currentDate         = df.parse((df.format(new Date())));
		//Date endDateFromString   = df.parse(lastEuqationEndTime);		

		//calculate time in seconds , and compare
		if(((currentDate.getTime() - endDateFromString.getTime()) / 1000) > 30)
			return true;


		/*} catch (java.text.ParseException e) {
			Log.e("DateTime", "inside isTimeDiffBetweenLastPlayQuestionAndCurrentTimeGreaterThen" +
					" Error while adding extraTime " + e.toString());
		}*/
		return false;
	}
}