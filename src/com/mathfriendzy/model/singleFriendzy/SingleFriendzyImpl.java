package com.mathfriendzy.model.singleFriendzy;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.mathfriendzy.database.Database;
import com.mathfriendzy.model.learningcenter.PlayerEquationLevelObj;

public class SingleFriendzyImpl 
{
	//private Context context 				= null;
	private SQLiteDatabase dbConn 	  	    = null;
	private Database database               = null;
	private String PLAYER_EQUATION_LEVEL    = "PlayerEquationLevel";

	private String PURCHASE_ITEMS           = "PurchasedItems";

	/**
	 * Constructor
	 * @param context
	 */
	public SingleFriendzyImpl(Context context)
	{
		database = new Database(context);
		database.open();		
	}

	/**
	 * This method open connection with database
	 */
	public void openConn()
	{
		dbConn = database.getConnection();
	}

	/**
	 * This method close the database conneciton
	 */
	public void closeConn()
	{
		if(dbConn != null)
			dbConn.close();
	}

	/**
	 * This method return the item id for unlock the level  if it is 11 then unlock level
	 * @param userId
	 * @return
	 */
	public int getItemId(String userId)
	{
		int itemId = 0;
		String query = "select Item_id from " + PURCHASE_ITEMS + " where User_id = '" + userId + "'" 
				+ " and Item_id = '11' ";

		Cursor cursor = dbConn.rawQuery(query, null);

		if(cursor.moveToNext())
			itemId = cursor.getInt(cursor.getColumnIndex("Item_id"));

		if(cursor != null)
			cursor.close();

		return itemId;
	}
	
	
	/**
	 * This method return true is exist other wise return false 
	 * @param userId
	 * @param player
	 * @param equationType
	 * @return
	 */
	public boolean isEquationPlayerExist(String userId , String playerId , int equationType)
	{

		boolean isExist = false;
		String query = "select * from " + PLAYER_EQUATION_LEVEL + " where PLAYER_ID = '" +  playerId + "'"
				+ "and  USER_ID = '" + userId + "' and EQUATION_TYPE = '"
				+ equationType + "'";


		Cursor cursor = dbConn.rawQuery(query, null);

		if(cursor.moveToNext())
			isExist =  true;
		else 
			isExist =  false; 

		if(cursor != null)
			cursor.close();
		//Log.e("Learnign Center Impl ", "inseide isEquationplayerExist query " + query + " boolean value " + isExist);

		return isExist;
	}

	/**
	 * This method update the complete level and stars
	 * @param userId
	 * @param playerId
	 * @param equationType
	 * @param completLevel
	 * @param stars
	 */
	public void updateEquaitonPlayerData(String userId , String playerId , int equationType , int completLevel , int stars)
	{
		String where = "USER_ID = '" + userId +"' and PLAYER_ID = '" + playerId 
				+ "' and EQUATION_TYPE = '" + equationType +"'" ;

		ContentValues cv = new ContentValues();

		cv.put("LEVEL", completLevel);
		cv.put("STARS", stars);

		dbConn.update(PLAYER_EQUATION_LEVEL, cv, where, null);
	}

	/**
	 * This method insert the data into player equaiton table
	 */
	public void insertIntoEquationPlayerTable(PlayerEquationLevelObj playrEquationObj)
	{

		ContentValues contentValues = new ContentValues();
		contentValues.put("USER_ID", playrEquationObj.getUserId());
		contentValues.put("PLAYER_ID", playrEquationObj.getPlayerId());
		contentValues.put("EQUATION_TYPE", playrEquationObj.getEquationType());
		contentValues.put("LEVEL", playrEquationObj.getLevel());
		contentValues.put("STARS", playrEquationObj.getStars());

		dbConn.insert(PLAYER_EQUATION_LEVEL, null, contentValues);
	}


	/***
	 * This method return data from player equations table
	 * @param userId
	 * @param playerId
	 * @param equationType
	 * @return
	 */
	public PlayerEquationLevelObj getEquaitonDataFromPlayerEquationTable(String userId , 
			String playerId , int equationType)
	{
		PlayerEquationLevelObj playrEquationObj = new PlayerEquationLevelObj();

		String query = "select * from " + PLAYER_EQUATION_LEVEL + " where PLAYER_ID = '" +  playerId + "'"
				+ "and  USER_ID = '" + userId + "' and EQUATION_TYPE = '"
				+ equationType + "'";

		Cursor cursor =  dbConn.rawQuery(query, null);

		while(cursor.moveToNext())
		{
			playrEquationObj.setUserId(cursor.getString(cursor.getColumnIndex("USER_ID")));
			playrEquationObj.setPlayerId(cursor.getString(cursor.getColumnIndex("PLAYER_ID")));
			playrEquationObj.setEquationType(cursor.getInt(cursor.getColumnIndex("EQUATION_TYPE")));
			playrEquationObj.setLevel(cursor.getInt(cursor.getColumnIndex("LEVEL")));
			playrEquationObj.setStars(cursor.getInt(cursor.getColumnIndex("STARS")));
		}
		//Log.e("", "star for single friendzy: "+playrEquationObj.getStars());
		if(cursor != null)
			cursor.close();
		return playrEquationObj;
	}


	/**
	 * This method return the item id for unlock the level  if it is 11 then unlock level
	 * @param userId
	 * @return
	 */
	public int getItemIdForMultiFriendzy(String userId)
	{
		int itemId = 0;
		String query = "select Item_id from " + PURCHASE_ITEMS + " where User_id = '" + userId + "'" 
				+ " and Item_id = '100' ";

		Cursor cursor = dbConn.rawQuery(query, null);

		if(cursor.moveToNext())
			itemId = cursor.getInt(cursor.getColumnIndex("Item_id"));

		if(cursor != null)
			cursor.close();

		return itemId;
	}
}
