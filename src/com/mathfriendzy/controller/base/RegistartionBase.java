package com.mathfriendzy.controller.base;

import java.util.ArrayList;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.mathfriendzy.model.learningcenter.LearningCenterimpl;
import com.mathfriendzy.model.learningcenter.PlayerTotalPointsObj;
import com.mathfriendzy.model.player.temp.TempPlayer;

public class RegistartionBase extends AdBaseActivity implements OnClickListener{

	protected void setWidgetsReferences() {

	}

	protected void setListenerOnWidgets() {
		// TODO Auto-generated method stub

	}

	protected void setTextFromTranslation() {
		// TODO Auto-generated method stub

	}
	/**
	 * Set hint to edit text
	 * @param edt
	 * @param hint
	 */
	protected void setHintToEditText(EditText edt , String hint){
		edt.setHint(hint);
	}
	/*protected void setHintToEditText(String text ,  EditText edtText){
		edtText.setHint(text);
	}*/
	/**
	 * Set hint to text
	 * @param text
	 * @param txtView
	 */
	protected void setHintToText(String text , TextView txtView){
		txtView.setText(text);
	}

	/**
	 * Enable disable listener
	 * @param enableDisable
	 */
	protected void enableDisableListenerOntextView(View view , boolean enableDisable){
		if(enableDisable){
			view.setOnClickListener(this);
		}else{
			view.setOnClickListener(null);
		}
	}

	@Override
	public void onClick(View v) {

	}

	/**
	 * This method return the xml format of the temp player data
	 * @param tempPlayerObj
	 * @return
	 */
	protected String getXmlPlayer(ArrayList<TempPlayer> tempPlayerObj){
		StringBuilder xml = new StringBuilder("");
		for( int i = 0 ;  i < tempPlayerObj.size() ; i++){

			PlayerTotalPointsObj playerObj = null;
			LearningCenterimpl learningCenterimpl = new LearningCenterimpl(this);
			learningCenterimpl.openConn();
			playerObj = learningCenterimpl.getDataFromPlayerTotalPoints
					(tempPlayerObj.get(i).getPlayerId() + "");
			learningCenterimpl.closeConn();
			xml.append("<player>" +
					"<playerId>"+tempPlayerObj.get(i).getPlayerId()+"</playerId>"+
					"<fName>"+tempPlayerObj.get(i).getFirstName()+"</fName>"+
					"<lName>"+tempPlayerObj.get(i).getLastName()+"</lName>"+
					"<grade>"+tempPlayerObj.get(i).getGrade()+"</grade>"+
					"<schoolId>"+tempPlayerObj.get(i).getSchoolId()+"</schoolId>"+
					"<teacherId>"+tempPlayerObj.get(i).getTeacherUserId()+"</teacherId>"+
					"<indexOfAppearance>-1</indexOfAppearance>"+
					"<profileImageId>"+tempPlayerObj.get(i).getProfileImageName()+"</profileImageId>"+
					"<coins>"+playerObj.getCoins()+"</coins>"+
					"<points>"+playerObj.getTotalPoints()+"</points>"+
					"<userName>"+tempPlayerObj.get(i).getUserName()+"</userName>"+
					"<competeLevel>"+playerObj.getCompleteLevel()+"</competeLevel>"+
					"</player>");
		}
		return xml.toString();
	}
	
	protected void setVisibilityOfStateAndZipCode(EditText edtZip , 
			Spinner spinnerState , RelativeLayout stateSpinnerLayout , 
			boolean isVisible){
		if(isVisible){
			edtZip.setVisibility(EditText.VISIBLE);
			spinnerState.setVisibility(Spinner.VISIBLE);
			stateSpinnerLayout.setVisibility(RelativeLayout.VISIBLE);
		}else{
			edtZip.setVisibility(EditText.GONE);
			spinnerState.setVisibility(Spinner.GONE);
			stateSpinnerLayout.setVisibility(RelativeLayout.GONE);
		}
	}
}
