package com.mathfriendzy.utils;

import android.content.Context;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;

import com.firstgradepaid.R;

/**
 * This class play sound
 * @author Yashwant Singh
 *
 */
public class PlaySound 
{
	private MediaPlayer mp ; 

	/**
	 * this method play sound when user give wrong answer
	 * @param languageCode
	 * @param context
	 */
	public void playSoundForWrong(Context context)
	{	
		try 
		{
			MediaPlayer mp = MediaPlayer.create(context, R.raw.incorrect_answer);
			if(mp != null)
				mp.start();
			mp.setOnCompletionListener(new OnCompletionListener() {
			    @Override
			    public void onCompletion(MediaPlayer mp) {
			    	mp.release();
			    }
			});

		} 
		catch (IllegalArgumentException e) 
		{
			e.printStackTrace();
		} 
		catch (IllegalStateException e) 
		{
			e.printStackTrace();
		} 
	}

	/**
	 * This method play sound when user give right answer
	 * @param languageCode
	 * @param context
	 */
	public void playSoundForRight(Context context)
	{	
		try 
		{
			MediaPlayer mp = MediaPlayer.create(context, R.raw.correct_answer);
			if(mp != null)
				mp.start();
			mp.setOnCompletionListener(new OnCompletionListener() {
			    @Override
			    public void onCompletion(MediaPlayer mp) {
			    	mp.release();
			    }
			});
		} 
		catch (IllegalArgumentException e) 
		{
			e.printStackTrace();
		} 
	}

	/**
	 * this method play sound for find challenger
	 * @param context
	 */
	public void playSoundForFindChallenger(Context context)
	{
		try 
		{
			mp = MediaPlayer.create(context, R.raw.loop_chordal_synth_pattern_16);

			mp.setLooping(true);
			if(mp != null)
				mp.start();
		} 
		catch (IllegalArgumentException e) 
		{
			e.printStackTrace();
		} 
		catch (IllegalStateException e) 
		{
			e.printStackTrace();
		} 
	}

	/**
	 * This method play sound when user play game
	 * @param context
	 */
	public void playSoundForBackground(Context context)
	{
		try
		{		
			mp = MediaPlayer.create(context, R.raw.trivia_background_music);
			mp.setLooping(true);
			if(mp != null)
				mp.start();
		} 
		catch (IllegalArgumentException e) 
		{
			e.printStackTrace();
		} 
		catch (IllegalStateException e) 
		{
			e.printStackTrace();
		} 
	}

	/**
	 * this method play sound when next question comes
	 * @param languageCode
	 * @param context
	 *//*
	public void playSoundForPageChange(Context context)
	{		
		try 
		{
			MediaPlayer mp = MediaPlayer.create(context, R.raw.page_peel);			
			//mp.prepare();
			//mp.prepareAsync();
			if(mp != null)
				mp.start();
			mp.setOnCompletionListener(new OnCompletionListener() {
			    @Override
			    public void onCompletion(MediaPlayer mp) {
			    	mp.release();
			    }
			});
		} 
		catch (IllegalArgumentException e) 
		{
			e.printStackTrace();
		} 
		catch (IllegalStateException e) 
		{
			e.printStackTrace();
		} 
	}*/

	/**
	 * this method play sound when user select a answer
	 * @param languageCode
	 * @param context
	 */
	public void playSoundForSelectingAnswer(Context context)
	{		
		try 
		{
			MediaPlayer mp = MediaPlayer.create(context, R.raw.select_answer);
			//mp.prepare();
			//mp.prepareAsync();
			if(mp != null)
				mp.start();
			mp.setOnCompletionListener(new OnCompletionListener() {
			    @Override
			    public void onCompletion(MediaPlayer mp) {
			    	mp.release();
			    }
			});
		} 
		catch (IllegalArgumentException e) 
		{
			e.printStackTrace();
		} 
		catch (IllegalStateException e) 
		{
			e.printStackTrace();
		} 
	}

	/**
	 * this method play sound for come to result screen
	 * @param languageCode
	 * @param context
	 */
	public void playSoundForResultScreen(Context context)
	{		
		try 
		{
			MediaPlayer mp = MediaPlayer.create(context, R.raw.results_screen);
			//mp.prepare();
			//mp.prepareAsync();
			if(mp != null)
				mp.start();
			mp.setOnCompletionListener(new OnCompletionListener() {
			    @Override
			    public void onCompletion(MediaPlayer mp) {
			    	mp.release();
			    }
			});
		} 
		catch (IllegalArgumentException e) 
		{
			e.printStackTrace();
		} 
		catch (IllegalStateException e) 
		{
			e.printStackTrace();
		}  
	}

	/**
	 * this method play sound for going away
	 * @param languageCode
	 * @param context
	 *//*
	public void playSoundForAnimation(Context context)
	{		
		try 
		{
			MediaPlayer mp = MediaPlayer.create(context, R.raw.answers_goingaway);
			//mp.prepare();
			//mp.prepareAsync();
			if(mp != null)
				mp.start();
			mp.setOnCompletionListener(new OnCompletionListener() {
			    @Override
			    public void onCompletion(MediaPlayer mp) {
			    	mp.release();
			    }
			});
		} 
		catch (IllegalArgumentException e) 
		{
			e.printStackTrace();
		} 
		catch (IllegalStateException e) 
		{
			e.printStackTrace();
		} 
	}*/

	/**
	 * This method stop player
	 */
	public void stopPlayer()
	{
		if(mp != null)
			mp.stop();		
	}
}
