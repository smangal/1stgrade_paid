package com.mathfriendzy.listener;

public interface OnRequestCompleteWithStringResult {
	void onComplete(String result);
}
