package com.mathfriendzy.model.learningcenter;

public class PurchaseItemObj 
{
	private String userId;
	private int itemId;
	private int status;
	
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public int getItemId() {
		return itemId;
	}
	public void setItemId(int item) {
		this.itemId = item;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
}
