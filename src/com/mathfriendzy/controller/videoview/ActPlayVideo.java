package com.mathfriendzy.controller.videoview;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.VideoView;

import com.firstgradepaid.R;
import com.mathfriendzy.controller.base.ActBase;
import com.mathfriendzy.controller.requestatutor.ActRequestATutor;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.serveroperation.HttpResponseBase;

public class ActPlayVideo extends ActBase {
	
	private VideoView videoView = null;
	private ImageView imgCross = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_act_play_video);
		
		this.setWidgetsReferences();
		this.setListenerOnWidgets();
		this.setVideoUri();
	}
	
	@Override
	protected void onStart() {		
		super.onStart();
	}
	
	@Override
	protected void onResume() {
		this.startVideo();
		super.onResume();
	}
	
	@Override
	protected void onPause() {
		this.stopVideo();
		super.onPause();
	}
	
	@Override
	protected void onStop() {		
		super.onStop();
	}
	
	private void setVideoUri(){
		String videoPath = MathFriendzyHelper.getSchoolAdVideUri(this);
		if(!MathFriendzyHelper.isEmpty(videoPath)){
			if(videoView != null){
				videoView.setVideoURI(Uri.parse(videoPath));				
			}
		}
	}
	
	private void startVideo(){
		if(videoView != null){
			videoView.start();
		}
	}
	
	private void stopVideo(){
		if(videoView != null){
			videoView.stopPlayback();
		}	
	}
	
	
	@Override
	public void onClick(View v) {
		switch(v.getId()){
		case R.id.imgCross:
			startActivity(new Intent(this , ActRequestATutor.class));
			this.finish();
			break;
		}		
	}

	@Override
	protected void setWidgetsReferences() {
		videoView = (VideoView) findViewById(R.id.videoView);
		imgCross = (ImageView) findViewById(R.id.imgCross);
	}

	@Override
	protected void setListenerOnWidgets() {
		imgCross.setOnClickListener(this);		
	}

	@Override
	protected void setTextFromTranslation() {
		// TODO Auto-generated method stub
		
	}	
	
	@Override
	public void serverResponse(HttpResponseBase httpResponseBase,
			int requestCode) {
		// TODO Auto-generated method stub
		
	}
}
