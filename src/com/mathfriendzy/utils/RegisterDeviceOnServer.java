package com.mathfriendzy.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;

import static com.mathfriendzy.utils.ICommonUtils.COMPLETE_URL_FOR_REG_DEVICE_NITFICATION;
import static com.mathfriendzy.utils.ICommonUtils.DEVICE_ID_PREFF;
import static com.mathfriendzy.utils.ICommonUtils.DEVICE_ID;

/**
 * This class register device on server for push notification
 * @author Yashwant Singh
 *
 */
public class RegisterDeviceOnServer extends AsyncTask<Void, Void, Void>
{	
	private String udid = null;
	private String regId= null;
	
	public RegisterDeviceOnServer(String regId , Context context)
	{	
		SharedPreferences sharedPreff = context.getSharedPreferences(DEVICE_ID_PREFF, 0);
		this.udid = sharedPreff.getString(DEVICE_ID, "");
		this.regId = regId;
	}
		
	@Override
	protected Void doInBackground(Void... params) 
	{
		registerDevice(udid , regId);
		return null;
	}
	
	/**
	 * This methid register device on server
	 * @param udid
	 * @param regId
	 */
	void registerDevice(String udid , String regId)
	{
		//Log.e("Device Register", "Registration id " + regId);
		String url = COMPLETE_URL_FOR_REG_DEVICE_NITFICATION + 
				"action=registerDevice&ud_id=" + udid + "&reg_id="+ regId +"&appId="
				+ CommonUtils.APP_ID;
		
		//Log.e("", "url : " + url);
		this.parseJsonForRegisterDeviceOnServer(CommonUtils.readFromURL(url));
	}
	
	/**
	 * This method parse json String
	 * @param jsonString
	 */
	private void parseJsonForRegisterDeviceOnServer(String jsonString)
	{
		//Log.e("RegisterDeviceOnServer", "jsonString : " + jsonString);
	}
}
