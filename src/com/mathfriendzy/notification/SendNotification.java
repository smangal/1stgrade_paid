package com.mathfriendzy.notification;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import com.mathfriendzy.utils.CommonUtils;
import com.mathfriendzy.utils.ICommonUtils;

import android.os.AsyncTask;
import android.util.Log;

/**
 * This class send notification to the devices
 * @author Yashwant Singh
 *
 */
public class SendNotification  extends AsyncTask<Void, Void, Void>
{
	private String TAG = this.getClass().getSimpleName();	
	
	private SendNotificationTransferObj sendNotificationObj;
	
	public SendNotification(SendNotificationTransferObj sendNotificationObj)
	{
		this.sendNotificationObj = sendNotificationObj;
	}
	
	@Override
	protected void onPreExecute() 
	{
		super.onPreExecute();
	}

	@Override
	protected Void doInBackground(Void... params) 
	{
		sendNotification(sendNotificationObj);
		return null;
	}
	
	@Override
	protected void onPostExecute(Void result) 
	{
		super.onPostExecute(result);
	}
	
	/**
	 * This method send notification to the devices
	 * @param sendNotificationObj
	 */
	private void sendNotification(SendNotificationTransferObj sendNotificationObj)
	{
		String encodeMessage = "";
		String encodeShareMesg = "";
		try 
		{
			encodeMessage   = URLEncoder.encode(sendNotificationObj.getMessage(), "UTF-8");
			encodeShareMesg = URLEncoder.encode(sendNotificationObj.getShareMessage(), "UTF-8");
			
		} 
		catch (UnsupportedEncodingException e) 
		{
			Log.e("Register", "Error:Unicode String Exception");
		}
		
		/*sendNotificationObj.getDevicePid()*/
		String action = "multifriendzyNotif";
	
		
		String strUrl    = ICommonUtils.COMPLETE_URL_FOR_REG_DEVICE_NITFICATION + "action=" + action + "&"
				+ "devicePids=" + sendNotificationObj.getDevicePid() + "&"
				+ "message="    + encodeMessage  + "&"
				+ "userId="     + sendNotificationObj.getUserId()    + "&"
				+ "playerId="   + sendNotificationObj.getPlayerId()  + "&"
				+ "country="    + sendNotificationObj.getCountry()   + "&"
				+ "oppUserId="  + sendNotificationObj.getOppUserId() + "&"
				+ "oppPlayerId="+ sendNotificationObj.getOppPlayerId()+ "&"
				+ "friendzyId=" + sendNotificationObj.getFriendzyId() + "&"
				+ "profileImgId="+ sendNotificationObj.getProfileImageId() + "&"
				+ "shareMsg="   + encodeShareMesg + "&"
				+ "senderDevicePids=" + sendNotificationObj.getSenderDeviceId();		
												
		//Log.e(TAG, "inside sendNotification url : " + strUrl);
		this.parseSendNotificationJson(CommonUtils.readFromURL(strUrl));		
		
		/*//for sending notification to iphone
		  String friendzyId = sendNotificationObj.getFriendzyId() + "@" + sendNotificationObj.getOppUserId() + "@"
		       + sendNotificationObj.getOppPlayerId() + "@" + sendNotificationObj.getProfileImageId() +"@"
		       + encodeShareMesg + "@" + sendNotificationObj.getSenderDeviceId();
		  
		  //sendNotificationObj.getIphoneDeviceIds()
		  String iphoneNotifiactionUrl = IPHONE_NOTIFICATION_URL + "task=call" + "&"
		          + "devicePids=" + sendNotificationObj.getIphoneDeviceIds() + "&"
		          + "userId="     + sendNotificationObj.getUserId()    + "&"
		          + "playerId="   + sendNotificationObj.getPlayerId()  + "&"
		          + "country="    + sendNotificationObj.getCountry()   + "&"
		          + "message="    + encodeMessage  + "&"
		          + "friendzyId=" + friendzyId;
		//send notification to iphone
		  this.parseSendNotificationJson(CommonUtils.readFromURL(iphoneNotifiactionUrl));*/
	}
	
	/**
	 * This method parse json for send notification
	 * @param jsonString
	 */
	private void parseSendNotificationJson(String jsonString)
	{
		//Log.e(TAG, "inside parseSendNotificationJson json String " + jsonString);
	}
}
