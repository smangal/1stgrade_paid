package com.mathfriendzy.controller.information;

import static com.mathfriendzy.utils.ITextIds.BTN_FEEDBACK;
import static com.mathfriendzy.utils.ITextIds.BTN_MORE_BOOKS;
import static com.mathfriendzy.utils.ITextIds.BTN_RATE;
import static com.mathfriendzy.utils.ITextIds.BTN_SHARE;
import static com.mathfriendzy.utils.ITextIds.FEEDBACK_ID;
import static com.mathfriendzy.utils.ITextIds.LBL_COPY_RIGHT;
import static com.mathfriendzy.utils.ITextIds.LBL_FEEDBACK;
import static com.mathfriendzy.utils.ITextIds.LBL_INFO;
import static com.mathfriendzy.utils.ITextIds.LBL_MORE_BOOKS;
import static com.mathfriendzy.utils.ITextIds.LBL_RATE;
import static com.mathfriendzy.utils.ITextIds.LBL_SHARE;
import static com.mathfriendzy.utils.ITextIds.MORE_BOOKS_URL;
import static com.mathfriendzy.utils.ITextIds.RATE_URL;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.firstgradepaid.R;
import com.mathfriendzy.controller.base.AdBaseActivity;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.utils.CommonUtils;

public class InformationActivity extends AdBaseActivity implements OnClickListener
{
	private Button btnRateUs			= null;
	private Button btnSendFeedback		= null;
	private Button btnShareApp			= null;
	private Button btnMoreBooks			= null;

	private TextView txtRateUs			= null;
	private TextView txtSendFeedback	= null;
	private TextView txtShareApp		= null;
	private TextView txtMoreBooks		= null;
	private TextView txtCopyRight		= null;
	private TextView txtTitleScreen		= null;

	/*private String feedback_subject		= null;
	private String share_subject		= null;
	private String share_body			= null;*/
	private TextView txtAppVersion = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_information);

		getWidgetId();
		setListener();
		setWidgetWithText();
	}//END onCreate method



	private void setListener() 
	{		
		btnRateUs.setOnClickListener(this);
		btnMoreBooks.setOnClickListener(this);
		btnSendFeedback.setOnClickListener(this);
		btnShareApp.setOnClickListener(this);

	}//END setListenerOnButton Method


	/**
	 * use to set text on view using Translation.
	 * @author Shilpi Mangal
	 */
	private void setWidgetWithText() 
	{		
		String text;
		Translation translate = new Translation(this);
		translate.openConnection();

		text = translate.getTranselationTextByTextIdentifier(BTN_FEEDBACK);
		btnSendFeedback.setText(text);

		text = translate.getTranselationTextByTextIdentifier(BTN_MORE_BOOKS);
		btnMoreBooks.setText(text);

		text = translate.getTranselationTextByTextIdentifier("lblRateMe");
		btnRateUs.setText(text);

		text = translate.getTranselationTextByTextIdentifier(BTN_SHARE);
		btnShareApp.setText(text);

		text = translate.getTranselationTextByTextIdentifier(LBL_COPY_RIGHT);
		txtCopyRight.setText(text);

		text = translate.getTranselationTextByTextIdentifier(LBL_FEEDBACK);
		txtSendFeedback.setText(text);

		text = translate.getTranselationTextByTextIdentifier(LBL_MORE_BOOKS);
		txtMoreBooks.setText(text);

		text = translate.getTranselationTextByTextIdentifier(LBL_RATE);
		txtRateUs.setText(text);

		text = translate.getTranselationTextByTextIdentifier(LBL_SHARE);
		txtShareApp.setText(text);

		text = translate.getTranselationTextByTextIdentifier(LBL_INFO);
		txtTitleScreen.setText(text);

		/*feedback_subject 	= translate.getTranselationTextByTextIdentifier(FEED_SUB);
		share_body 			= translate.getTranselationTextByTextIdentifier(EMAIL_BODY);
		share_subject 		= translate.getTranselationTextByTextIdentifier(EMAIL_SUB);*/
		txtAppVersion.setText(translate.getTranselationTextByTextIdentifier("lblAppVersion")
                + ":" + MathFriendzyHelper.getAppVersion(this) + MathFriendzyHelper
                .getAppVersionTextForTesting(this));
		translate.closeConnection();
	}//END setWidgetWithText Method



	private void getWidgetId() 
	{
		btnRateUs		= (Button) findViewById(R.id.btnRateUs);
		btnMoreBooks	= (Button) findViewById(R.id.btnMoreBooks);
		btnSendFeedback	= (Button) findViewById(R.id.btnSendFeedback);
		btnShareApp		= (Button) findViewById(R.id.btnShareApp);

		txtRateUs		= (TextView) findViewById(R.id.txtRateUs);
		txtCopyRight	= (TextView) findViewById(R.id.txtCopyRight);
		txtMoreBooks	= (TextView) findViewById(R.id.txtMoreBooks);
		txtSendFeedback	= (TextView) findViewById(R.id.txtSendFeedback);
		txtShareApp		= (TextView) findViewById(R.id.txtShareApp);		
		txtTitleScreen	= (TextView) findViewById(R.id.txtTitleScreen);

		txtAppVersion = (TextView) findViewById(R.id.txtAppVersion);
	}//END getWidgetId Method



	@Override
	public void onClick(View v)
	{
		Intent intent = null;
		switch(v.getId())
		{
		case R.id.btnRateUs:
			intent = new Intent(Intent.ACTION_VIEW, Uri.parse(RATE_URL));
			startActivity(intent);
			break;

		case R.id.btnSendFeedback:
			Intent i = new Intent(Intent.ACTION_SEND);
			i.setType("message/rfc822");

			i.putExtra(Intent.EXTRA_EMAIL  , new String[] {FEEDBACK_ID});

			i.putExtra(Intent.EXTRA_SUBJECT, MathFriendzyHelper.getAppName(this) + " - Android");
			try 
			{
				startActivity(Intent.createChooser(i, "Send mail..."));
			} catch (android.content.ActivityNotFoundException ex) {
				Toast.makeText(this, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
			}
			break;

		case R.id.btnShareApp:
			intent = new Intent(Intent.ACTION_SEND);
			intent.setType("message/rfc822");

			intent.putExtra(Intent.EXTRA_SUBJECT, "Check Out " + MathFriendzyHelper.getAppName(this));
			intent.putExtra(Intent.EXTRA_TEXT, Html.fromHtml(CommonUtils.getMessageForShareMail()));
			try 
			{
				startActivity(Intent.createChooser(intent, "Send mail..."));
			} catch (android.content.ActivityNotFoundException ex) {
				Toast.makeText(this, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
			}
			break;

		case R.id.btnMoreBooks:
			intent = new Intent(Intent.ACTION_VIEW, Uri.parse(MORE_BOOKS_URL));
			startActivity(intent);
			break;
		}

	}//END onClick method	



}
