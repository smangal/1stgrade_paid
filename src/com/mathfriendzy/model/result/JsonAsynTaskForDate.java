package com.mathfriendzy.model.result;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

import com.mathfriendzy.controller.result.SelectDateActivity;
import com.mathfriendzy.utils.CommonUtils;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import static com.mathfriendzy.utils.ICommonUtils.COMPLETE_URL;

public class JsonAsynTaskForDate extends AsyncTask<Void, Void, String> 
{
	Context context;
	int userid;
	int playerid;
	String playerName;
	int offset;
	ProgressDialog progressDialog;
	
	public JsonAsynTaskForDate(Context context, int userid,
			int playerid, String playerName, int offset){
		
		this.context 	= context;
		this.userid 	= userid;
		this.playerid 	= playerid;
		this.playerName = playerName;
		this.offset		= offset;
		
		progressDialog = CommonUtils.getProgressDialog(context);
		progressDialog.show();
	}
	
	/**
	 * USE TO PARSE JSON FOR DATES
	 * @return FILE IN STRING
	 */
	private String getJsonFile()
	{
		String str = "";
		StringBuilder stringBuilder = new StringBuilder("");
		URL url = null;
		try {

			url = new URL(COMPLETE_URL + "action=getMathScoreDates&userId="
					+userid+"&playerId="+playerid+"&offset="+offset+"&appId=7");

			BufferedReader ins = new BufferedReader(new InputStreamReader(url.openStream()));				
			try 
			{     
				while((str = ins.readLine()) != null)
				{				
					stringBuilder.append(str);
				}
			} 
			catch (IOException e) 
			{
				e.printStackTrace();
			}

		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return stringBuilder.toString();
	}

	
	@Override
	protected String doInBackground(Void... params)
	{		
		return getJsonFile();
	}

	
	@Override
	protected void onPostExecute(String result) 
	{
		Intent intent = new Intent(context, SelectDateActivity.class);
		intent.putExtra("jsonFile", result);
		intent.putExtra("userId", userid);
		intent.putExtra("playerId", playerid);
		intent.putExtra("playerName" , playerName);
		intent.putExtra("offset", offset);
		progressDialog.cancel();
		context.startActivity(intent);
		
		super.onPostExecute(result);
	}
	
}
