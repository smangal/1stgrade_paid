package com.mathfriendzy.model.login;

import static com.mathfriendzy.utils.ICommonUtils.COMPLETE_URL;

import java.net.URLEncoder;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;

import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.model.learningcenter.LearningCenterimpl;
import com.mathfriendzy.model.learningcenter.PlayerTotalPointsObj;
import com.mathfriendzy.model.registration.RegistereUserDto;
import com.mathfriendzy.model.registration.UserPlayerDto;
import com.mathfriendzy.model.registration.UserPlayerOperation;
import com.mathfriendzy.model.registration.UserRegistrationOperation;
import com.mathfriendzy.utils.CommonUtils;
import com.mathfriendzy.utils.ICommonUtils;

/**
 * This class hold the login operation
 * @author Yashwant Singh
 *
 */
public class Login 
{
	public static int SUCCESS       = 2;//login user success
	public static int INVALID_EMAIL = 1;//invalid email
	private int flag = 0;
	private ArrayList<UserPlayerDto> playerList = null;
	private RegistereUserDto regUserObj 		= null;

	private Context context = null;

	/**
	 * Constructor
	 * @param context
	 */
	public Login(Context context) 
	{
		this.context = context;
	}

	/**
	 * Get login Data By Email
	 * @param email
	 * @param pass
	 * @return
	 */
	public int getLoginDataByEmail(String email,String pass)
	{			
		String action = "login";
		String strUrl = COMPLETE_URL + "action=" + action + "&"
				+ "email=" 	    + 	email	+ "&"
				+ "password="	+	pass +"&appId=7&appName=1stGrade";

		int result =  this.parseLoginEmailJson(CommonUtils.readFromURL(strUrl));

		if(result == SUCCESS)
		{
			UserRegistrationOperation userObj = new UserRegistrationOperation(context);
			UserPlayerOperation userPlayerObj = new UserPlayerOperation(context);
			if(userObj.isUserTableExist())
			{
				userObj.insertUserData(regUserObj);
				userPlayerObj.deleteFromUserPlayer();
				userPlayerObj.insertUserPlayerData(playerList);
			}
			else
			{
				userObj.createUserTable(regUserObj);
				userPlayerObj.createUserPlayerTable(playerList);
			}

			this.insertDataIntoPlayerTotalPointsTable();
		}
		return result;
	}

	/**
	 * This method insert data into player total point table
	 */
	private void insertDataIntoPlayerTotalPointsTable()
	{
		LearningCenterimpl learningCenterImpl = new LearningCenterimpl(context);
		learningCenterImpl.openConn();
		learningCenterImpl.deleteFromPlayerTotalPointsExceptTempPlayerData();

		for( int i = 0 ; i < playerList.size() ; i ++ )
		{
			PlayerTotalPointsObj playerPoints = new PlayerTotalPointsObj();
			playerPoints.setUserId(playerList.get(i).getParentUserId());
			playerPoints.setPlayerId(playerList.get(i).getPlayerid());

			try
			{
				playerPoints.setTotalPoints(Integer.parseInt(playerList.get(i).getPoints()));
				playerPoints.setCoins(Integer.parseInt(playerList.get(i).getCoin()));
				playerPoints.setCompleteLevel(Integer.parseInt(playerList.get(i).getCompletelavel()));
			}
			catch(Exception e)
			{
				playerPoints.setTotalPoints(0);
				playerPoints.setCoins(0);
				playerPoints.setCompleteLevel(0);
			}

			learningCenterImpl.insertIntoPlayerTotalPoints(playerPoints);
		}
		learningCenterImpl.closeConn();
	}


	/**
	 * Get Login Data By UserName
	 * @param userName
	 * @param pass
	 * @return
	 */
	public int getLoginDataByUserName(String userName,String pass)
	{
		try{
			userName = URLEncoder.encode(userName);
			pass = URLEncoder.encode(pass);
		}catch(Exception e){
			e.printStackTrace();
		}
		
		String action = "LoginStudent";
		String strUrl = COMPLETE_URL + "action=" + action + "&"
				+ "username="   + 	userName	+ "&"
				+ "password="	+	pass +"&appId=7&appName=1stGrade";		

		int result =  this.parseLoginEmailJson(CommonUtils.readFromURL(strUrl));

		if(result == SUCCESS)
		{
			UserRegistrationOperation userObj = new UserRegistrationOperation(context);
			UserPlayerOperation userPlayerObj = new UserPlayerOperation(context);
			if(userObj.isUserTableExist())
			{
				userObj.insertUserData(regUserObj);
				userPlayerObj.deleteFromUserPlayer();
				userPlayerObj.insertUserPlayerData(playerList);

			}
			else
			{
				userObj.createUserTable(regUserObj);
				userPlayerObj.createUserPlayerTable(playerList);
			}

			this.insertDataIntoPlayerTotalPointsTable();
		}
		return result;
	}

	/**
	 * This method parse login data from the server
	 * @param jsonString
	 * @return
	 */
	private int parseLoginEmailJson(String jsonString)
	{

		if(ICommonUtils.LOG_ON)
			Log.e("Login", "inside parseLoginEmailJson response " + jsonString);

		int resultValue = 0;
		playerList = new ArrayList<UserPlayerDto>();
		regUserObj = new RegistereUserDto();

		UserPlayerDto userPlayerObj = null;

		JSONObject jObject = null;
		JSONObject jsonObject2 = null;
		try 
		{
			jObject = new JSONObject(jsonString);

			try
			{
				jsonObject2 = jObject.getJSONObject("data");
				flag = 0;
			}
			catch(JSONException e1)
			{
				flag = 1;
				String code = jObject.getString("code");
				if(code.equals("-9001"))
					resultValue = INVALID_EMAIL;
			}

			if(flag == 0)
			{
				resultValue = SUCCESS;
				regUserObj.setFirstName(jsonObject2.getString("fname"));
				regUserObj.setUserId(jsonObject2.getString("userId"));
				regUserObj.setVolume(jsonObject2.getString("volume"));
				regUserObj.setSchoolId(jsonObject2.getString("schoolId"));
				regUserObj.setSchoolName(jsonObject2.getString("schoolName"));
				regUserObj.setPreferedLanguageId(jsonObject2.getString("preferredLanguageId"));
				regUserObj.setLastName(jsonObject2.getString("lname"));
				regUserObj.setEmail(jsonObject2.getString("email"));
				regUserObj.setPass(jsonObject2.getString("password"));
				regUserObj.setIsParent(jsonObject2.getString("isParent"));
				regUserObj.setCountryId(jsonObject2.getString("countryId"));
				regUserObj.setStateId(jsonObject2.getString("stateId"));
				regUserObj.setState(jsonObject2.getString("state"));
				regUserObj.setCity(jsonObject2.getString("city"));
				regUserObj.setCoins(jsonObject2.getString("coins"));
				regUserObj.setZip(jsonObject2.getString("zip"));

				try{
					if(MathFriendzyHelper.checkForKeyExistInJsonObj(jsonObject2, "expireDate"))
						MathFriendzyHelper.saveSubscriptionExpireDate
						(context, jsonObject2.getString("expireDate"));
				}catch(Exception e){
					e.printStackTrace();
				}

				/*try{
                    String emailSubscription = jsonObject2.getString("emailSubscription");
                    String emailValid = jsonObject2.getString("emailValid");
                    MathFriendzyHelper.saveEmailSubscriptionAndValid(context , 
                    		emailSubscription , emailValid);
                }catch(Exception e){
                    e.printStackTrace();
                }*/

				JSONArray jsonPlayerObj = jsonObject2.getJSONArray("players");
				for( int i = 0 ; i < jsonPlayerObj.length() ; i++)
				{
					JSONObject jsonObject = jsonPlayerObj.getJSONObject(i);

					userPlayerObj = new UserPlayerDto();

					userPlayerObj.setFirstname(jsonObject.getString("fName"));
					userPlayerObj.setLastname(jsonObject.getString("lName"));
					userPlayerObj.setSchoolId(jsonObject.getString("schoolId"));
					userPlayerObj.setSchoolName(jsonObject.getString("schoolName"));
					userPlayerObj.setGrade(jsonObject.getString("grade"));
					userPlayerObj.setTeacherUserId(jsonObject.getString("teacherUserId"));
					userPlayerObj.setTeacherFirstName(jsonObject.getString("teacherFirstName"));
					userPlayerObj.setTeacheLastName(jsonObject.getString("teacherLastName"));
					userPlayerObj.setIndexOfAppearance(jsonObject.getString("indexOfAppearance"));
					userPlayerObj.setParentUserId(jsonObject.getString("parentUserId"));
					userPlayerObj.setPlayerid(jsonObject.getString("playerId"));
					userPlayerObj.setCompletelavel(jsonObject.getString("competeLevel"));
					userPlayerObj.setProfileImage(jsonObject.getString("profileImageId").getBytes());//changes
					userPlayerObj.setImageName(jsonObject.getString("profileImageId"));//changes
					userPlayerObj.setCoin(jsonObject.getString("coins"));
					userPlayerObj.setPoints(jsonObject.getString("points"));
					userPlayerObj.setCity(jsonObject.getString("city"));
					userPlayerObj.setStateName(jsonObject.getString("state"));
					userPlayerObj.setUsername(jsonObject.getString("userName"));
					if(MathFriendzyHelper.checkForKeyExistInJsonObj(jsonObject, "studentIdByTeacher"))
						userPlayerObj.setStudentIdByTeacher(jsonObject.getString("studentIdByTeacher"));
					playerList.add(userPlayerObj);
				}

				try{
					if(MathFriendzyHelper.checkForKeyExistInJsonObj(jObject, "isAdsDisable")){
						MathFriendzyHelper.saveIsAdDisble(context ,
								jObject.getInt("isAdsDisable"));
					}
				}catch(Exception e){
					e.printStackTrace();
				}

				try{
					if(MathFriendzyHelper.checkForKeyExistInJsonObj(jObject, "subscriptionDate")){
						MathFriendzyHelper.savePurchaseSubscriptionDate(context ,
								jObject.getString("subscriptionDate"));
					}
				}catch(Exception e){
					e.printStackTrace();
				}
			}	
		}
		catch (JSONException e) 
		{			
			e.printStackTrace();
			return 0;
		}
		return resultValue;
	}

	/**
	 * This method send the password to the email and return the response
	 * @param email
	 * @return
	 */
	public String forgetPassword(String email)
	{
		String action = "forgotPassword";
		String strUrl = COMPLETE_URL + "action=" + action + "&"
				+ "email=" + email+"&appId=7&appName=1stGrade";
		String resultValue = this.parseForgetPasswordJson(CommonUtils.readFromURL(strUrl));

		return resultValue;
	}

	private String parseForgetPasswordJson(String jsonString)
	{
		String result = "";
		try 
		{
			JSONObject jObject = new JSONObject(jsonString);
			result = jObject.getString("data");
		}
		catch (JSONException e) 
		{			
			e.printStackTrace();
		}
		return result;
	}


	/**
	 * Get User Data By Email Id
	 * @param userName
	 * @param pass
	 * @return
	 */
	public int getUserDetailByEmail(String email,String pass)
	{
		String action = "getUserDetails";
		String strUrl = COMPLETE_URL + "action=" + action + "&"
				+ "email="   + 	email	+ "&"
				+ "password="	+	pass +"&appId=7&appName=1stGrade";

		int result =  this.parseLoginEmailJson(CommonUtils.readFromURL(strUrl));

		if(result == SUCCESS)
		{
			UserRegistrationOperation userObj = new UserRegistrationOperation(context);
			UserPlayerOperation userPlayerObj = new UserPlayerOperation(context);
			if(userObj.isUserTableExist())
			{
				userObj.insertUserData(regUserObj);
				userPlayerObj.deleteFromUserPlayer();
				userPlayerObj.insertUserPlayerData(playerList);

			}
			else
			{
				userObj.createUserTable(regUserObj);
				userPlayerObj.createUserPlayerTable(playerList);
			}
		}
		return result;
	}

	/**
	 * Get User Data By UseriD
	 * @param userName
	 * @param pass
	 * @return
	 */
	public int getUserDetailByUserId(String userId)
	{
		String action = "getStudentsInfo";
		String strUrl = COMPLETE_URL + "action=" + action + "&"
				+ "userId="   + 	userId+"&appId=7&appName=1stGrade" ;


		int result =  this.parseLoginEmailJson(CommonUtils.readFromURL(strUrl));

		if(result == SUCCESS)
		{
			UserRegistrationOperation userObj = new UserRegistrationOperation(context);
			UserPlayerOperation userPlayerObj = new UserPlayerOperation(context);
			if(userObj.isUserTableExist())
			{
				userObj.insertUserData(regUserObj);
				userPlayerObj.deleteFromUserPlayer();
				userPlayerObj.insertUserPlayerData(playerList);

			}
			else
			{
				userObj.createUserTable(regUserObj);
				userPlayerObj.createUserPlayerTable(playerList);
			}
		}
		return result;
	}

	/**
	 * This method get purchase avtar from server
	 * @param userId
	 */
	public ArrayList<PurchasedAvtarDto> getPurchasedAvtar(String userId)
	{
		String action = "getPurchasedAvatars";
		String strUrl = COMPLETE_URL + "action=" + action + "&"
				+ "userId="   + 	userId ;

		//Log.e("Login", "inside getPurchasedAvtar url : " + strUrl);
		return this.parseGetPurchasedAvtar(CommonUtils.readFromURL(strUrl));
	}

	/**
	 * This meh\thod parse the json for getAvtar status
	 * @param jsonString
	 * @return
	 */
	private ArrayList<PurchasedAvtarDto> parseGetPurchasedAvtar(String jsonString)
	{
		//Log.e("Login", "inside parseGetPurchasedAvtar jsonString " + jsonString);
		ArrayList<PurchasedAvtarDto> purchaseAvtarList = new ArrayList<PurchasedAvtarDto>();

		try
		{
			JSONObject jsonObj = new JSONObject(jsonString);
			JSONArray purchaseArray = jsonObj.getJSONArray("data");

			for( int  i = 0 ; i < purchaseArray.length() ; i ++ )
			{
				JSONObject avtadData = purchaseArray.getJSONObject(i);

				PurchasedAvtarDto purchase = new PurchasedAvtarDto();
				purchase.setActerId(avtadData.getString("avatarId"));
				purchase.setUserId(avtadData.getString("uid"));
				purchase.setPlayerId(avtadData.getString("pid"));
				purchase.setStatus(avtadData.getInt("status"));

				purchaseAvtarList.add(purchase);
			}
		}
		catch(JSONException e)
		{
			Log.e("Login", "inside parseGetPurchasedAvtar Error while parsing : " + e.toString());			
		}

		return purchaseAvtarList;
	}

	/**
	 * This method save the purchase item ids for temp player on server
	 * @param userId
	 * @param purchasedItemIds
	 */
	public void savePurchasedItem(String userId , String purchasedItemIds)
	{
		String action = "savePurchasedItemsForUser";
		String strUrl = COMPLETE_URL + "action=" + action + "&"
				+ "userId="   + 	userId + "&"
				+ "purchasedItemsIds=" + purchasedItemIds+"&appId="+CommonUtils.APP_ID;

		//Log.e("Login", "inside savePurchasedItem url : " + strUrl);
		this.parseSavePurchaseItemResult(CommonUtils.readFromURL(strUrl));
	}

	private void parseSavePurchaseItemResult(String jsonString)
	{
		//Log.e("Login", "inside savePurchasedItem jsonString : " + jsonString);
	}

}
